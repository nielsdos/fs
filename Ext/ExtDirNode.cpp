/*
 * MIT License
 *
 * Copyright (c) 2018 The Copper authors and contributors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <FileSystem/Ext/Features.hpp>
#include <FileSystem/Ext/FS.hpp>
#include <FileSystem/Ext/ExtDirNode.hpp>
#include <Compiler.hpp>

namespace FileSystem::Ext
{

ExtDirNode::ExtDirNode(FS* fs, uint32_t inodeNum, Inode* inode)
    : ExtNode(fs, inodeNum, inode)
{
}

#if 0
Node* ExtDirNode::Open(const char* name)
{
    // TODO: handle trivial entries
    std::cout << "need to search for: "<<name<<std::endl;

    uint32_t block = FindBlock(0);
    if(unlikely(block == 0))
    {
        errno = ENOENT;
        return nullptr;
    }

    //HTreeRoot* root = (HTreeRoot*);

    // TODO: verify hashversion, verify string length

    //off_t start = __builtin_offsetof(HTreeRoot, Reserved) + root->GetInfoLength();


    return nullptr;
}
#endif
#if 1
Node* ExtDirNode::Open(const char* name)
{
    uint32_t blockSize = _FS->GetBlockSize();

    uint8_t* buffer = new uint8_t[blockSize];
    if(unlikely(!buffer))
    {
        errno = ENOMEM;
        return nullptr;
    }

    uint32_t blockId  = 0;
    uint32_t blockOff = 0;
    uint32_t blockNum = FindBlock(blockId);
    if(unlikely(blockNum == 0 || !_FS->ReadBlockDevice(buffer, blockNum, 1)))
    {
        errno = ENOENT;
        delete[] buffer;
        return nullptr;
    }

    /*
     * Loop through entries.
     * The new offset is always at most the size of this directory.
     * So that means: no need to check for out of bounds.
     */
    off_t left = GetSize();
    bool found = false;
    while(left > 0)
    {
        ExtDirEnt* dirent = (ExtDirEnt*)(buffer + blockOff);
        if(!strncmp(name, dirent->Name, dirent->Namelen))
        {
            found = true;
            break;
        }

        left     -= dirent->GetReclen();
        blockOff += dirent->GetReclen();

        // We need to read a new block if we are out of bounds
        if(blockOff >= blockSize)
        {
            blockId++;
            blockOff -= blockSize;
            blockNum = FindBlock(blockId);

            if(unlikely(blockNum == 0 || !_FS->ReadBlockDevice(buffer, blockNum, 1)))
                break;
        }
    }

    ExtDirEnt* dirent = (ExtDirEnt*)(buffer + blockOff);
    uint32_t ino = dirent->GetInode();
    delete[] buffer;

    if(!found)
    {
        errno = ENOENT;
        return nullptr;
    }

    return _FS->GetNodeFromInode(ino);
}
#endif

off_t ExtDirNode::Seek(off_t oldOff, off_t newOff)
{
    /*
     * We are in a directory and the user attempts to move the offset.
     * That means the user may move us to an invalid entry start.
     * We must verify and adjust the offset.
     */
    newOff = Node::Seek(oldOff, newOff);
    if(oldOff == newOff)
        return oldOff;

    // Fast path for going to begin of directory, no need to verify that
    if(newOff == 0)
        return newOff;

    /*
     * There's no good way to seek backwards in directories.
     * Since we would need to know all the previous offsets.
     * Which means that we need to start at the start of the directory...
     */
    off_t currentOff;
    if(oldOff < newOff)
        currentOff = oldOff;
    else /* if(oldOff > newOff) */
        currentOff = 0;

    uint32_t blockSize = _FS->GetBlockSize();

    uint8_t* buffer = new uint8_t[blockSize];
    if(!buffer)
        return oldOff;

    uint32_t blockId  = currentOff / blockSize;
    uint32_t blockOff = currentOff % blockSize;
    uint32_t blockNum = FindBlock(blockId);
    if(unlikely(blockNum == 0 || !_FS->ReadBlockDevice(buffer, blockNum, 1)))
    {
        delete[] buffer;
        return oldOff;
    }

    /*
     * Loop through entries.
     * The new offset is always at most the size of this directory.
     * So that means: no need to check if out of bounds.
     */
    while(currentOff < newOff)
    {
        ExtDirEnt* dirent = (ExtDirEnt*)(buffer + blockOff);
        blockOff += dirent->GetReclen();
        currentOff += dirent->GetReclen();

        // We need to read a new block if we are out of bounds
        if(blockOff >= blockSize)
        {
            blockId++;
            blockOff -= blockSize;
            blockNum = FindBlock(blockId);

            if(unlikely(blockNum == 0 || !_FS->ReadBlockDevice(buffer, blockNum, 1)))
                break;
        }
    }

    delete[] buffer;

    return currentOff;
}

ssize_t ExtDirNode::ReadDirEntries(DirEnt* dirents, size_t count, off_t& offset)
{
    off_t    fileSize  = GetSize();
    uint32_t blockSize = _FS->GetBlockSize();

    // The count is at most equal to the file size
    if(count > (size_t)fileSize - offset)
        count = fileSize - offset;

    uint8_t* buffer = new uint8_t[blockSize];
    if(!buffer)
    {
        errno = ENOMEM;
        return -1;
    }

    uint32_t blockId  = offset / blockSize;
    uint32_t blockOff = offset % blockSize;
    uint32_t blockNum = FindBlock(blockId);

    if(unlikely(blockNum == 0 || !_FS->ReadBlockDevice(buffer, blockNum, 1)))
    {
        delete[] buffer;
        return 0;
    }

    // Loop through entries
    size_t read = 0;
    while(read < count)
    {
        ExtDirEnt* dirent = (ExtDirEnt*)(buffer + blockOff);
        if(unlikely(dirent->Inode == 0))
            break;

        dirents->Offset = offset;
        dirents->Inode  = dirent->GetInode();
        dirents->Type   = GetTypeFromDirent(dirent->Type);
        dirents->Reclen = CalcAlignedDirEntReclen(dirent->Namelen);

        /*
         * The "Namelen" field is an uint8_t, so the maximum length is 255.
         * The Name field is 256 bytes long, 255 bytes + 1 NULL byte
         * So that means it's safe to put "Namelen + 1".
         */
        strlcpy(dirents->Name, dirent->Name, dirent->Namelen + 1);

        blockOff += dirent->GetReclen();
        offset   += dirent->GetReclen();
        read     += dirents->Reclen;
        dirents   = (DirEnt*)((uint8_t*)dirents + dirents->Reclen);

        // We need to read a new block if we are out of bounds
        if(blockOff >= blockSize)
        {
            blockId++;
            blockOff -= blockSize;
            blockNum = FindBlock(blockId);

            if(unlikely(blockNum == 0 || !_FS->ReadBlockDevice(buffer, blockNum, 1)))
                break;
        }
    }

    delete[] buffer;
    return read;
}

}