/*
 * MIT License
 *
 * Copyright (c) 2018 The Copper authors and contributors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <atomic>
#include <cassert>
#include <string.h>
#include <errno.h>
#include <FileSystem/VFS.hpp>
#include <Panic.hpp>
#include <Utils/Logger.hpp>
#include <Compiler.hpp>

namespace FileSystem
{

static NodeContainer RootFS;
static NodeContainer DevFS;
static NodeContainer VolumesFS;

void InitVFS()
{
    if(!RootFS.Init() || !DevFS.Init() || !VolumesFS.Init())
        Runtime::Panic("Could not initialize VFS");

    Node* RootFSRoot    = RootFS.GetRoot();
    Node* DevFSRoot     = DevFS.GetRoot();
    Node* VolumesFSRoot = VolumesFS.GetRoot();

    RootFS.Add("..", RootFSRoot);
    RootFS.Add("Devices", DevFSRoot);
    RootFS.Add("Volumes", VolumesFSRoot);

    DevFS.Add("..", RootFSRoot);

    VolumesFS.Add("..", RootFSRoot);
}

uint16_t NewMajorDevNumber()
{
    static std::atomic<uint16_t> n = { 1 };
    return n++;
}

bool Sync()
{
    return VolumesFS.Sync();
}

Node* OpenInternal(Node* node, const char* name)
{
    assert(node);

    char* start = strdup(name);
    if(!start)
    {
        errno = ENOMEM;
        return nullptr;
    }

    bool  searching = true;
    char* current   = start;

    while(searching)
    {
        char* part = current;

        /*
         * There's two possibilities:
         *   1) This was the last part, so there are no more '/'.
         *      There's only a '\0' to mark the end.
         *   2) This was not the last part, so there's a '/' to mark the end.
         */
        uint32_t i = 0;
        for(; part[i] != '/'; i++)
        {
            if(part[i] == '\0')
            {
                searching = false;
                break;
            }
        }

        /*
         * If this was not the last part, we need to split.
         * Then proceed to next node.
         */
        if(*part != '\0')
        {
            Node* old = node;

            current = part + i + 1;
            current[-1] = '\0';

            node = old->Open(part);
            old->Close();

            if(node == nullptr)
                searching = false;
        }
    }

    // TODO: re-enable me (caused link error)
    //free(start);
    return node;
}

Node* Open(Node* node, const char* name)
{
    assert(name[0] != '/');

    // Note: increase count because this gets closed by the OpenInternal method
    node->IncRefCount();
    return OpenInternal(node, name);
}

Node* Open(const char* name)
{
    assert(name[0] == '/');

    // Note: this gets closed by the OpenInternal method
    return OpenInternal(RootFS.GetRoot(), name + 1);
}

/**
 * Adds a node to a container
 * @param container The container
 * @param prefix The name of the container to put as a prefix
 * @param name The name
 * @param node The node
 * @return The error status
 */
static ContainerError AddNodeToContainer(NodeContainer& container, const char* prefix, const char* name, Node* node)
{
    ContainerError error = container.Add(name, node);

    switch(error)
    {
        default:
        case ContainerError::OK:
            Logger::Info("VFS: %s '%s' added", prefix, name);
            break;

        case ContainerError::INVALID_NAME:
            Logger::Warn("VFS: %s name '%s' is invalid", prefix, name);
            break;

        case ContainerError::DUPLICATE_ENTRY:
            Logger::Warn("VFS: %s name '%s' is already used", prefix, name);
            break;
    }

    return error;
}

/**
 * Removes a node from a container
 * @param container The container
 * @param prefix The name of the container to put as a prefix
 * @param name The name
 * @return True if successfully removed
 */
static bool RemoveNodeFromContainer(NodeContainer& container, const char* prefix, const char* name)
{
    ContainerError error = container.Remove(name);

    switch(error)
    {
        default:
        case ContainerError::OK:
            Logger::Info("VFS: %s '%s' removed", prefix, name);
            return true;

        case ContainerError::INVALID_NAME:
            Logger::Warn("VFS: %s name '%s' is invalid", prefix, name);
            break;

        case ContainerError::NO_ENTRY:
            Logger::Warn("VFS: %s '%s' not found", prefix, name);
            break;
    }

    return false;
}

ContainerError AddVolume(const char* name, IFileSystem* fs)
{
    Node* root = fs->GetRoot();
    assert(root);

    ContainerError error = AddNodeToContainer(VolumesFS, "Volume", name, root);
    if(error != ContainerError::OK)
        root->Close();

    return error;
}

ContainerError AddDevice(const char* name, Node* node)
{
    return AddNodeToContainer(DevFS, "Device", name, node);
}

bool RemoveVolume(const char* name)
{
    return RemoveNodeFromContainer(VolumesFS, "Volume", name);
}

bool RemoveDevice(const char* name)
{
    return RemoveNodeFromContainer(DevFS, "Device", name);
}

dev_t MakeDev(uint16_t major, uint32_t minor)
{
    return ((major & 0xFFF) << 20) | (minor & 0xFFFFF);
}

uint16_t GetMajorFromDev(dev_t dev)
{
    return (dev >> 20);
}

uint32_t GetMinorFromDev(dev_t dev)
{
    return (dev & 0xFFFFF);
}

}
