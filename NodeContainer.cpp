/*
 * MIT License
 *
 * Copyright (c) 2018 The Copper authors and contributors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <assert.h>
#include <string.h>
#include <FileSystem/NodeContainer.hpp>
#include <FileSystem/DirEnt.hpp>
#include <FileSystem/VFS.hpp>
#include <Sync/ScopedLock.hpp>
#include <Compiler.hpp>

namespace FileSystem
{

/**
 * Checks if the given name is a valid name
 * @param name The name
 * @return True if the name is valid
 */
static bool IsValidName(const char* name)
{
    // Name must not be empty and must not contain a '/'
    return (name[0] != '\0' && strchr(name, '/') == nullptr);
}

NodeContainer::ContainerRoot::ContainerRoot(NodeContainer* fs)
    : _FS(fs)
{
    _type = DT_DIR;
    _ino  = _currentIno++;
}

bool NodeContainer::ContainerRoot::Init()
{
    return (_nodes.Init() && _nodes.Add(".", this));
}

Node* NodeContainer::ContainerRoot::Open(const char* name)
{
    Node* res;
    if(!_nodes.Get(name, res))
        return nullptr;

    res->IncRefCount();
    return res;
}

off_t NodeContainer::ContainerRoot::GetSize() const
{
    return _nodes.GetSize();
}

ssize_t NodeContainer::ContainerRoot::ReadDirEntries(DirEnt* dirents, size_t count, off_t& offset)
{
    _lock.Lock();

    /*
     * Offset is FS dependent and FS internal.
     * So for convencience, we use index offset.
     */
    auto end  = _nodes.end();
    auto it   = _nodes.begin();
    for(off_t i = 0; i < offset; i++)
        ++it;

    size_t read = 0;
    DirEnt* entries = dirents;
    while(read < count && it != end)
    {
        auto entry = *it;
        Node* node = entry.Value;
        const char* name = entry.Key;

        size_t namelen  = strlen(name);
        uint16_t reclen = CalcAlignedDirEntReclen(namelen);

        entries->Inode  = node->GetIno();
        entries->Offset = offset;
        entries->Reclen = reclen;
        entries->Type   = node->GetType();
        strlcpy(entries->Name, name, NAME_MAX);

        entries = (DirEnt*)((uint8_t*)entries + reclen);
        read += reclen;
        offset++;
        ++it;
    }

    _lock.Unlock();
    return read;
}

const IFileSystem* NodeContainer::ContainerRoot::GetFS() const
{
    return _FS;
}

ContainerError NodeContainer::ContainerRoot::Add(const char* name, Node* node)
{
    Sync::ScopedLock lock(_lock);

    Node* tmp;
    if(_nodes.Get(name, tmp))
        return ContainerError::DUPLICATE_ENTRY;

    node->SetIno(_currentIno++);
    node->SetFS(_FS);

    if(!_nodes.Add(name, node))
        return ContainerError::NO_MEM;

    return ContainerError::OK;
}

bool NodeContainer::ContainerRoot::Remove(const char* name)
{
    Sync::ScopedLock lock(_lock);

    Node* tmp;
    if(!_nodes.Get(name, tmp))
        return false;

    _nodes.Remove(name);

    return true;
}

bool NodeContainer::ContainerRoot::Sync()
{
    _lock.Lock();

    bool good = true;
    for(const auto& entry : _nodes)
    {
        // Let's not do this...
        if(unlikely(entry.Value == this))
            continue;

        auto fs = entry.Value->GetFS();
        assert(fs);
        if(!fs->Sync())
            good = false;
    }

    _lock.Unlock();
    return good;
}

NodeContainer::NodeContainer()
{
    _deviceNumber = MakeDev(NewMajorDevNumber(), 0);
}

bool NodeContainer::Sync()
{
    return _root.Sync();
}

dev_t NodeContainer::GetDevNumber() const
{
    return _deviceNumber;
}

ContainerError NodeContainer::Add(const char* name, Node* node)
{
    assert(node);

    if(!IsValidName(name))
        return ContainerError::INVALID_NAME;

    return _root.Add(name, node);
}

ContainerError NodeContainer::Remove(const char* name)
{
    if(!IsValidName(name))
        return ContainerError::INVALID_NAME;

    if(!_root.Remove(name))
        return ContainerError::NO_ENTRY;

    return ContainerError::OK;
}

}