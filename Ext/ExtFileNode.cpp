/*
 * MIT License
 *
 * Copyright (c) 2018 The Copper authors and contributors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <FileSystem/Ext/Features.hpp>
#include <FileSystem/Ext/FS.hpp>
#include <FileSystem/Ext/ExtFileNode.hpp>
#include <Compiler.hpp>

namespace FileSystem::Ext
{

ExtFileNode::ExtFileNode(FS* fs, uint32_t inodeNum, Inode* inode)
    : ExtNode(fs, inodeNum, inode)
{
}

ssize_t ExtFileNode::PRead(void* buffer, size_t count, off_t offset)
{
    off_t fileSize = GetSize();
    uint32_t blockSize = _FS->GetBlockSize();

    // The count is at most equal to the file size
    if(count > (size_t)fileSize - offset)
        count = fileSize - offset;

    uint32_t blockId  = offset / blockSize;
    uint32_t blockOff = offset % blockSize;
    uint32_t blockNum = FindBlock(blockId);

    ssize_t read = 0;

    // Read non-blocksize-aligned part
    if(blockOff != 0)
    {
        uint8_t* tmp = new uint8_t[blockSize];
        if(!tmp)
            return 0;

        if(unlikely(!_FS->ReadBlockDevice(tmp, blockNum, 1)))
        {
            delete[] tmp;
            return 0;
        }

        size_t toCopy = (count < blockSize - blockOff) ? count : (blockSize - blockOff);
        memcpy(buffer, tmp + blockOff, toCopy);

        blockId++;
        read += toCopy;
        blockNum = FindBlock(blockId);

        delete[] tmp;
    }

    // Read block sized parts
    while(count - read >= blockSize)
    {
        if(unlikely(blockNum == 0 || !_FS->ReadBlockDevice((uint8_t*)buffer + read, blockNum, 1)))
            return read;

        // We need to read a new block if we are out of bounds
        blockId++;
        blockNum = FindBlock(blockId);
        read += blockSize;
    }

    // Small part left
    if(count - read > 0 && blockNum != 0)
    {
        uint8_t* tmp = new uint8_t[blockSize];
        if(!tmp)
            return read;

        if(likely(_FS->ReadBlockDevice(tmp, blockNum, 1)))
        {
            memcpy((uint8_t*)buffer + read, tmp, count - read);
            read += count - read;
        }
        
        delete[] tmp;
    }

    return read;
}

}