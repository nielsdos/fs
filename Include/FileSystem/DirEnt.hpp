/*
 * MIT License
 *
 * Copyright (c) 2018 The Copper authors and contributors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <stdint.h>
#include <sys/types.h>

namespace FileSystem
{

enum
{
    DT_UNKNOWN = 0,
    DT_FIFO    = 1,
    DT_CHR     = 2,
    DT_DIR     = 4,
    DT_BLK     = 6,
    DT_REG     = 8,
    DT_LNK     = 10,
    DT_SOCK    = 12
};

// Note: Name includes NULL character
struct DirEnt
{
    ino_t          Inode;
    off_t          Offset;
    unsigned short Reclen;
    unsigned char  Type;
    char           Name[];
};

constexpr size_t PATH_MAX = 4096;
constexpr size_t NAME_MAX = 256;

/**
 * Aligns the dirent reclen so the pointer becomes aligned
 * @param x The current reclen
 * @return The aligned reclen
 */
inline uint16_t AlignDirEntReclen(uint8_t x)
{
    return (x + sizeof(size_t) - 1) & -sizeof(size_t);
}

/**
 * Calculates the aligned dirent reclen from a name length
 * @param n The name length
 * @return The aligned dirent reclen
 */
inline uint16_t CalcAlignedDirEntReclen(uint8_t n)
{
    return (AlignDirEntReclen(__builtin_offsetof(DirEnt, Name)) + AlignDirEntReclen(n));
}

}