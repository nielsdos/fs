/*
 * MIT License
 *
 * Copyright (c) 2018 The Copper authors and contributors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <FileSystem/Node.hpp>
#include <FileSystem/Partition/GPT.hpp>
#include <FileSystem/Partition/MBR.hpp>
#include <Utils/CRC32.hpp>
#include <Utils/Logger.hpp>

namespace FileSystem::Partition
{

struct GPTHeader
{
    uint64_t Signature;         // Should be "EFI PART"
    uint32_t Revision;
    uint32_t HeaderSize;
    uint32_t HeaderCRC32;       // CRC32 of this header (during calculation this is set to zero)
    uint32_t Reserved;
    uint64_t MyLBA;             // Current location of this header copy
    uint64_t BackupLBA;         // Location of backup header copy
    uint64_t FirstUsableLBA;
    uint64_t LastUsableLBA;     // (inclusive)
    char     GUID[16];
    uint64_t PartitionEntryLBA; // LBA of array of partition entries
    uint32_t PartitionCount;    // Number of partition entires in array
    uint32_t EntrySize;         // Size of a single partition entry
    uint32_t CRC32Array;        // CRC32 of partition array
    uint8_t  Reserved2[420];    // Could also be larger if the sector sizes are bigger
};

static_assert(sizeof(GPTHeader) == 512);

struct GPTPartition
{
    char     TypeGUID[16];      // Partition type GUID
    char     PartitionGUID[16]; // Unique partition GUID
    uint64_t FirstLBA;
    uint64_t LastLBA;           // (inclusive)
    uint64_t Flags;
    uint16_t PartitionName[36]; // Note: 36 UTF-16 code units
};

GPT::GPT(Node* blockDevice)
    : IPartitionTable(blockDevice)
{
    if(!Init())
        return;

    uint32_t blockSize = _blockDevice->GetBlockSize();

    GPTHeader gpt;
    if(blockDevice->PRead(&gpt, sizeof(gpt), blockSize) != sizeof(gpt))
        return;

    #if __BYTE_ORDER == __LITTLE_ENDIAN
    const uint64_t expectedSignature = 0x5452415020494645;
    #else
    const uint64_t expectedSignature = 0x4546492050415254;
    #endif
    if(gpt.Signature != expectedSignature)
        return;

    // During calculation, the CRC32 is set to zero
    uint32_t expectedCRC32 = gpt.HeaderCRC32;
    gpt.HeaderCRC32 = 0;
    if(expectedCRC32 != Utils::CRC32(&gpt, 92))
    {
        Logger::Warn("GPT: Invalid checksum");
        return;
    }

    // Decode, everything is in little endian
    _firstLBA       = le64toh(gpt.FirstUsableLBA);
    _lastLBA        = le64toh(gpt.LastUsableLBA);
    _entriesLBA     = le64toh(gpt.PartitionEntryLBA);
    _partitionCount = le32toh(gpt.PartitionCount);
    _CRC32Entries   = le32toh(gpt.CRC32Array);
    _entrySize      = le32toh(gpt.EntrySize);

    // Sane?
    if(_firstLBA > _lastLBA)
    {
        Logger::Warn("GPT: First LBA (%lx) > Last LBA (%lx)", _firstLBA, _lastLBA);
        return;
    }

    if(_entriesLBA >= _firstLBA && _entriesLBA <= _lastLBA)
    {
        Logger::Warn("GPT: Partition entries LBA is invalid (%lx), First LBA: %lx, Last LBA: %lx", _entriesLBA, _firstLBA, _lastLBA);
        return;
    }

    _valid = ParsePartitions();
}

bool GPT::ParsePartitions()
{
    uint32_t blockSize = _blockDevice->GetBlockSize();

    // Stack may overflow if too big, which is why we use the heap
    size_t   tableSize = _entrySize * _partitionCount;
    uint8_t* parts     = new uint8_t[tableSize];
    if(!parts)
        return false;

    if(_blockDevice->PRead(parts, tableSize, _entriesLBA * blockSize) != (ssize_t)tableSize)
    {
        delete[] parts;
        return false;
    }

    // Validate
    if(Utils::CRC32(parts, tableSize) != _CRC32Entries)
    {
        Logger::Warn("GPT: CRC32 of partition table is invalid");
        delete[] parts;
        return false;
    }

    for(uint32_t i = 0; i < _partitionCount; i++)
    {
        // Actual size may be bigger than our struct
        GPTPartition* part = (GPTPartition*)(parts + (i * _entrySize));

        uint64_t first = le64toh(part->FirstLBA);
        uint64_t last  = le64toh(part->LastLBA);
        if(first < _firstLBA || first > _lastLBA ||
            last < _firstLBA || last > _lastLBA ||
            last < first)
            continue;

        /*
         * Overflow check.
         * Note: we don't check for zero because the previous condition doesn't let zero's through.
         */
        if(first > OFF_T_MAX / blockSize ||
           (last - first + 1) > OFF_T_MAX / blockSize)
        {
            continue;
        }

        Partition partition =
        {
            .Type = PartitionType::PRIMARY,
            .Num = _currentID++,
            .Start = (off_t)first * blockSize,
            .Length = (off_t)(last - first + 1) * blockSize
        };

        AddPartition(partition);
    }

    delete[] parts;
    return true;
}

bool GPT::IsPossible(MBR& mbr)
{
    return (mbr.HasEFISystemPartition() && mbr.GetPartitionCount() == 1);
}

}