/*
 * MIT License
 *
 * Copyright (c) 2018 The Copper authors and contributors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <FileSystem/Stat.hpp>
#include <FileSystem/Ext/Features.hpp>
#include <FileSystem/Ext/FS.hpp>
#include <FileSystem/Ext/ExtNode.hpp>
#include <Compiler.hpp>
//#include <iostream>

namespace FileSystem::Ext
{

ExtNode::ExtNode(FS* fs, uint32_t inodeNum, Inode* inode)
    : _FS(fs), _data(inode)
{
    //std::cout << "Flags: "<<inode->GetFlags()<<std::endl;
    _ino  = inodeNum;
    _type = GetTypeFromMode(inode->GetMode());
}

ExtNode::~ExtNode()
{
    delete _data;
}

unsigned char ExtNode::GetTypeFromMode(uint16_t mode)
{
    mode &= 0xF000;
    switch(mode)
    {
        case S_IFREG:  return DT_REG;
        case S_IFDIR:  return DT_DIR;
        case S_IFIFO:  return DT_FIFO;
        case S_IFSOCK: return DT_SOCK;
        case S_IFCHR:  return DT_CHR;
        case S_IFBLK:  return DT_BLK;
        case S_IFLNK:  return DT_LNK;
    }
    return DT_UNKNOWN;
}

unsigned char ExtNode::GetTypeFromDirent(FileType type)
{
    switch(type)
    {
        case FileType::REG:  return DT_REG;
        case FileType::DIR:  return DT_DIR;
        case FileType::FIFO: return DT_FIFO;
        case FileType::SOCK: return DT_SOCK;
        case FileType::CHR:  return DT_CHR;
        case FileType::BLK:  return DT_BLK;
        case FileType::LNK:  return DT_LNK;
        default:             return DT_UNKNOWN;
    }
}

uint32_t ExtNode::GetBlockSize() const
{
    return _FS->GetBlockSize();
}

const IFileSystem* ExtNode::GetFS() const
{
    return _FS;
}

off_t ExtNode::GetSize() const
{
    uint64_t size = (uint64_t)_data->GetSizeLo();
    if(_FS->HasROCompat(ROCompatFlag::LARGE_FILE))
        size |= (uint64_t)_data->GetSizeHi() << 32;

    if(unlikely(size > OFF_T_MAX))
        return OFF_T_MAX;

    return size;
}

void ExtNode::DecodeTime(struct timespec& ts, uint32_t base, uint32_t extra) const
{
    bool extraBits = (_FS->GetInodeSize() >= sizeof(Inode));

    ts.tv_sec = base;
    if(extraBits)
    {
        if(sizeof(ts.tv_sec) > 4)
            ts.tv_sec |= (uint64_t)(extra & 3) << 32;

        ts.tv_nsec = extra >> 2;
    }
}

void ExtNode::GetAccessTime(struct timespec& ts) const
{
    DecodeTime(ts, _data->GetATime(), _data->GetATimeExtra());
}

void ExtNode::GetModifiedTime(struct timespec& ts) const
{
    DecodeTime(ts, _data->GetMTime(), _data->GetMTimeExtra());
}

void ExtNode::GetChangeTime(struct timespec& ts) const
{
    DecodeTime(ts, _data->GetCTime(), _data->GetCTimeExtra());
}

void ExtNode::GetCreationTime(struct timespec& ts) const
{
    if(_FS->GetInodeSize() >= sizeof(Inode))
        DecodeTime(ts, _data->GetCRTime(), _data->GetCRTimeExtra());
}

void ExtNode::GetStat(Stat* buf) const
{
    Node::GetStat(buf);

    buf->Mode   = _data->GetMode();
    buf->UID    = _data->GetUID();
    buf->GID    = _data->GetGID();
    buf->NLink  = _data->GetLinksCount();
    buf->Blocks = _data->GetBlocksLo();

    if(_FS->HasROCompat(ROCompatFlag::HUGE_FILE))
        buf->Blocks |= (uint64_t)_data->GetBlocksHi() << 32;
    if(_data->GetFlags() & HUGE_FILE_FL)
        buf->Blocks *= _FS->GetBlockSize() / 512;

    GetAccessTime(buf->AccessTime);
    GetModifiedTime(buf->ModifiedTime);
    GetChangeTime(buf->ChangeTime);
    GetCreationTime(buf->CreationTime);
}

uint32_t ExtNode::FindBlock(uint32_t block) const
{
    /*
     * 0 - 11 => Direct map
     * 12 - (BlockSize / 4) + 11 => Singly indirect block
     * ... - (BlockSize / 4)² + (BlockSize / 4) + 11 => Doubly indirect block
     * ... - (BlockSize / 4)³ + (BlockSize / 4)² + (BlockSize / 4) + 12 => Triply indirect block
     */
    const uint32_t bs4 = _FS->GetBlockSize() / 4;
//TODO: use RAII techniques for automatically deleting the pointer
    if(block < 12)
    {
        return le32toh(_data->Blocks[block]);
    }
    else if(block < 12 + bs4)
    {
        uint32_t* tmp = new uint32_t[bs4];
        if(!tmp)
            return 0;

        if(unlikely(!_FS->ReadBlockDevice(tmp, le32toh(_data->Blocks[12]), 1)))
        {
            delete[] tmp;
            return 0;
        }

        uint32_t ret = le32toh(tmp[block - 12]);
        delete[] tmp;
        return ret;
    }
    else if(block < 12 + bs4 * (bs4 + 1))
    {
        uint32_t* tmp = new uint32_t[bs4];
        if(!tmp)
            return 0;

        block -= 12 + bs4;
        uint32_t doubly = block / bs4;
        uint32_t singly = block % bs4;

        // Indirect map for doubly
        if(unlikely(!_FS->ReadBlockDevice(tmp, le32toh(_data->Blocks[13]), 1)))
        {
            delete[] tmp;
            return 0;
        }

        // Read doubly entry
        uint32_t ret = le32toh(tmp[doubly]);
        if(unlikely(!_FS->ReadBlockDevice(tmp, ret, 1)))
        {
            delete[] tmp;
            return 0;
        }

        ret = le32toh(tmp[singly]);
        delete[] tmp;
        return ret;
    }
    else if(block < 12 + bs4 * (bs4 * (bs4 + 1) + 1))
    {
        uint32_t* tmp = new uint32_t[bs4];
        if(!tmp)
            return 0;

        block -= 12 + bs4 * (bs4 + 1);
        uint32_t triply = block / (bs4 * bs4);
        uint32_t remain = block % (bs4 * bs4);
        uint32_t doubly = remain / bs4;
        uint32_t singly = remain % bs4;

        // Indirect map for triply
        if(unlikely(!_FS->ReadBlockDevice(tmp, le32toh(_data->Blocks[14]), 1)))
        {
            delete[] tmp;
            return 0;
        }

        // Read triply entry
        uint32_t ret = le32toh(tmp[triply]);
        if(unlikely(!_FS->ReadBlockDevice(tmp, ret, 1)))
        {
            delete[] tmp;
            return 0;
        }

        // Read doubly entry
        ret = le32toh(tmp[doubly]);
        if(unlikely(!_FS->ReadBlockDevice(tmp, ret, 1)))
        {
            delete[] tmp;
            return 0;
        }

        ret = le32toh(tmp[singly]);
        delete[] tmp;
        return ret;
    }

    return 0;
}

}