/*
 * MIT License
 *
 * Copyright (c) 2018 The Copper authors and contributors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <FileSystem/Ext/Hash.hpp>

namespace FileSystem::Ext
{

// Basic MD4 functions
#define MD4_F(x, y, z) (z ^ (x & (y ^ z)))
#define MD4_G(x, y, z) ((x & y) + ((x ^ y) & z))
#define MD4_H(x, y, z) (x ^ y ^ z)
// Rotates x left n bits
#define ROT_LEFT(x, n) ((x << n) | (x >> (32 - n)))
// Generic round function
#define ROUND(f, a, b, c, d, x, s) \
    a += f(b, c, d) + x; \
    a = ROT_LEFT(a, s)

#define FF(a, b, c, d, x, s) ROUND(MD4_F, a, b, c, d, x + 0x00000000, s)
#define GG(a, b, c, d, x, s) ROUND(MD4_G, a, b, c, d, x + 0x5A827999, s)
#define HH(a, b, c, d, x, s) ROUND(MD4_H, a, b, c, d, x + 0x6ED9EBA1, s)

/**
 * Converts a string to a hash buffer, common version
 * Note: The highest bit can cause the ubsan to trigger, but we depend on this behaviour of losing bits
 * @param str The string
 * @param strLen The string length
 * @param blocks The destination blocks
 * @param blockCount The block count
 */
template<typename T>
[[gnu::no_sanitize_undefined]]
inline static void StringToHashBuffer(const T* __restrict str, uint8_t strLen, uint32_t* __restrict blocks, uint32_t blockCount)
{
    // Cap to protect against outside bounds
    if(strLen > blockCount * sizeof(uint32_t))
        strLen = blockCount * sizeof(uint32_t);

    uint32_t fullBlocks = strLen / sizeof(uint32_t);
    for(uint32_t i = 0; i < fullBlocks; i++)
    {
        blocks[i] = (str[0] << 24) + (str[1] << 16) + (str[2] << 8) + (str[3]);
        str += 4;
    }

    if(fullBlocks < blockCount)
    {
        uint32_t padding = strLen;
        padding |= padding << 8;
        padding |= padding << 16;

        uint32_t remaining = strLen % 4;
        uint32_t val = padding;
        while(remaining-- > 0)
            val = (val << 8) + *str++;

        blocks[fullBlocks] = val;

        for(uint32_t i = fullBlocks + 1; i < blockCount; i++)
            blocks[i] = padding;
    }
}

/**
 * Converts a string to a hash buffer, signed version
 * @param str The string
 * @param strLen The string length
 * @param blocks The destination blocks
 * @param blockCount The block count
 */
void StringToHashBufferSigned(const char* __restrict str, uint8_t strLen, uint32_t* __restrict blocks, uint32_t blockCount)
{
    StringToHashBuffer<char>(str, strLen, blocks, blockCount);
}

/**
 * Converts a string to a hash buffer, unsigned version
 * @param str The string
 * @param strLen The string length
 * @param blocks The destination blocks
 * @param blockCount The block count
 */
void StringToHashBufferUnsigned(const unsigned char* __restrict str, uint8_t strLen, uint32_t* __restrict blocks, uint32_t blockCount)
{
    StringToHashBuffer<unsigned char>(str, strLen, blocks, blockCount);
}

/**
 * Calculates a hash using the legacy hash function, common version
 * @param str The string
 * @param strLen The string length
 * @return The hash
 */
template<typename T>
inline static uint32_t LegacyHash(const T* str, uint8_t strLen)
{
    uint32_t prev = 0x37ABE8F9;
    uint32_t hash = 0x12A3FE2D;

    while(strLen-- > 0)
    {
        uint32_t next = prev + (hash ^ (*str++ * 7152373));
        if(next & 0x80000000)
            next -= 0x7FFFFFFF;

        prev = hash;
        hash = next;
    }

    return hash << 1;
}

/**
 * Calculates a hash using the legacy hash function, signed version
 * @param str The string
 * @param strLen The string length
 * @return The hash
 */
uint32_t LegacyHashSigned(const char* str, uint8_t strLen)
{
    return LegacyHash<char>(str, strLen);
}

/**
 * Calculates a hash using the legacy hash function, unsigned version
 * @param str The string
 * @param strLen The string length
 * @return The hash
 */
uint32_t LegacyHashUnsigned(const unsigned char* str, uint8_t strLen)
{
    return LegacyHash<unsigned char>(str, strLen);
}

/**
 * Calculates a TEA hash
 * @param buffer The buffer
 * @param blocks The string blocks
 */
void TEAHash(uint32_t buffer[4], const uint32_t blocks[4])
{
    uint32_t v0 = buffer[0];
    uint32_t v1 = buffer[1];

    uint32_t k0 = blocks[0];
    uint32_t k1 = blocks[1];
    uint32_t k2 = blocks[2];
    uint32_t k3 = blocks[3];

    uint32_t sum = 0;
    for(int i = 0; i < 16; i++)
    {
        sum += 0x9E3779B9;
        v0  += ((v1 << 4) + k0) ^ (v1 + sum) ^ ((v1 >> 5) + k1);
        v1  += ((v0 << 4) + k2) ^ (v0 + sum) ^ ((v0 >> 5) + k3);
    }

    buffer[0] += v0;
    buffer[1] += v1;
}

/**
 * Calculates a _HALF_ MD4 hash
 * @param buffer The buffer
 * @param blocks The string blocks
 */
void HalfMD4Hash(uint32_t buffer[4], const uint32_t blocks[8])
{
    uint32_t a = buffer[0];
    uint32_t b = buffer[1];
    uint32_t c = buffer[2];
    uint32_t d = buffer[3];

    // Round 1
    FF(a, b, c, d, blocks[0],  3);
    FF(d, a, b, c, blocks[1],  7);
    FF(c, d, a, b, blocks[2], 11);
    FF(b, c, d, a, blocks[3], 19);
    FF(a, b, c, d, blocks[4],  3);
    FF(d, a, b, c, blocks[5],  7);
    FF(c, d, a, b, blocks[6], 11);
    FF(b, c, d, a, blocks[7], 19);

    // Round 2
    GG(a, b, c, d, blocks[1],  3);
    GG(d, a, b, c, blocks[3],  5);
    GG(c, d, a, b, blocks[5],  9);
    GG(b, c, d, a, blocks[7], 13);
    GG(a, b, c, d, blocks[0],  3);
    GG(d, a, b, c, blocks[2],  5);
    GG(c, d, a, b, blocks[4],  9);
    GG(b, c, d, a, blocks[6], 13);

    // Round 3
    HH(a, b, c, d, blocks[3],  3);
    HH(d, a, b, c, blocks[7],  9);
    HH(c, d, a, b, blocks[2], 11);
    HH(b, c, d, a, blocks[6], 15);
    HH(a, b, c, d, blocks[1],  3);
    HH(d, a, b, c, blocks[5],  9);
    HH(c, d, a, b, blocks[0], 11);
    HH(b, c, d, a, blocks[4], 15);

    buffer[0] += a;
    buffer[1] += b;
    buffer[2] += c;
    buffer[3] += d;
}

// TODO: move to btree file
uint32_t CalculateHash(const char* str, uint8_t strLen)
{
    DirHash algo = DirHash::HALF_MD4;//TODO
    bool isSigned = true;
    uint32_t buffer[4] = { 0x67452301, 0xEFCDAB89, 0x98BADCFE, 0x10325476 };

    uint32_t hash;
    switch(algo)
    {
        case DirHash::LEGACY:
            hash = (isSigned) ? LegacyHashSigned(str, strLen) : LegacyHashUnsigned((const unsigned char*)str, strLen);
            break;

        case DirHash::HALF_MD4:
        {
            for(int len = strLen; len > 0; len -= 32)
            {
                uint32_t blocks[8];
                if(isSigned)
                    StringToHashBufferSigned(str, (uint8_t)len, blocks, 8);
                else
                    StringToHashBufferUnsigned((const unsigned char*)str, (uint8_t)len, blocks, 8);
                HalfMD4Hash(buffer, blocks);
                str += 32;
            }

            hash = buffer[1];
            break;
        }

        case DirHash::TEA:
        {
            for(int len = strLen; len > 0; len -= 16)
            {
                uint32_t blocks[4];
                if(isSigned)
                    StringToHashBufferSigned(str, (uint8_t)len, blocks, 4);
                else
                    StringToHashBufferUnsigned((const unsigned char*)str, (uint8_t)len, blocks, 4);
                TEAHash(buffer, blocks);
                str += 16;
            }

            hash = buffer[0];
            break;
        }
    }

    // One bit is used as a flag to see if there are multiple entries with this hash
    return hash & ~1;
}

}