/*
 * MIT License
 *
 * Copyright (c) 2018 The Copper authors and contributors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <FileSystem/Node.hpp>

namespace FileSystem
{

class IFileSystem;

namespace Partition
{

struct Partition;

class PartitionNode : public Node
{
public:
    /**
     * Creates a new partition node from partition information
     * @param part The partition information
     * @param blockDevice The block device
     */
    PartitionNode(const Partition& part, Node* blockDevice);

    ssize_t PRead(void* buffer, size_t count, off_t offset);
    ssize_t PWrite(const void* buffer, size_t count, off_t offset);
    uint32_t GetBlockSize() const;
    off_t GetSize() const;
    dev_t GetDevNumber() const;

    const IFileSystem* GetFS() const;
    void SetFS(IFileSystem* fs);

private:
    IFileSystem* _FS;
    Node*        _blockDevice;
    off_t        _start;
    off_t        _length;
    dev_t        _devNumber;
};

}

}