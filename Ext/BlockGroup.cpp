/*
 * MIT License
 *
 * Copyright (c) 2018 The Copper authors and contributors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <errno.h>
#include <string.h>
#include <malloc.h>
#include <FileSystem/Ext/BlockGroup.hpp>
#include <FileSystem/Ext/ExtDirNode.hpp>
#include <FileSystem/Ext/ExtFileNode.hpp>
#include <FileSystem/Ext/FS.hpp>
#include <FileSystem/Ext/Inode.hpp>

namespace FileSystem::Ext
{

BlockGroup::BlockGroup(FS* fs, uint32_t id, BlockGroupDesc* bgd)
    : _FS(fs), _ID(id), _data(bgd)
{
    /*
     * Calculate some values that we use often so we don't have to constantly calculate
     */
    _inodeTable = bgd->GetInodeTableLo();
    if(fs->Is64())
    {
        _inodeTable |= (uint64_t)bgd->GetInodeTableHi() << 32;
    }
}

BlockGroup::~BlockGroup()
{
    delete _data;
}

ExtNode* BlockGroup::GetNodeFromInode(uint32_t id)
{
    // Frequently used
    const uint32_t blockSize = _FS->GetBlockSize();
    const uint16_t inodeSize = _FS->GetInodeSize();

    Inode* inode = (Inode*)malloc(inodeSize);
    if(!inode)
    {
        errno = ENOMEM;
        return nullptr;
    }

    // Offset of inode in this block group
    uint32_t index  = (id - 1) % _FS->GetInodesPerGroup();
    uint32_t offset = (index * inodeSize) % blockSize;
    uint64_t block  = _inodeTable + (index * inodeSize / blockSize);

    uint8_t* buffer = new uint8_t[blockSize];
    if(!buffer)
    {
        errno = ENOMEM;
        delete inode;
        return nullptr;
    }

    if(!_FS->ReadBlockDevice(buffer, block, 1))
    {
        errno = ENOENT;
        delete inode;
        delete[] buffer;
        return nullptr;
    }

    memcpy(inode, buffer + offset, inodeSize);
    delete[] buffer;

    auto ret = (S_ISDIR(inode->GetMode()))
                ? static_cast<ExtNode*>(new ExtDirNode(_FS, id, inode))
                : static_cast<ExtNode*>(new ExtFileNode(_FS, id, inode));

    if(!ret)
        errno = ENOMEM;

    return ret;
}

void BlockGroup::Sync()
{
    // TODO
}

}