/*
 * MIT License
 *
 * Copyright (c) 2018 The Copper authors and contributors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

namespace FileSystem::Ext
{

/*
 * Used in Inode->Mode.
 */
enum
{
    S_IXOTH  = 0x1,
    S_IWOTH  = 0x2,
    S_IROTH  = 0x4,
    S_IXGRP  = 0x8,
    S_IWGRP  = 0x10,
    S_IRGRP  = 0x20,
    S_IXUSR  = 0x40,
    S_IWUSR  = 0x80,
    S_IRUSR  = 0x100,
    S_ISVTX  = 0x200,
    S_ISGID  = 0x400,
    S_ISUID  = 0x800,
    // The following are mutually-exclusive file types
    S_IFIFO  = 0x1000,
    S_IFCHR  = 0x2000,
    S_IFDIR  = 0x4000,
    S_IFBLK  = 0x6000,
    S_IFREG  = 0x8000,
    S_IFLNK  = 0xA000,
    S_IFSOCK = 0xC000
};

/*
 * Inode flags.
 */
enum
{
    SECRM_FL        = 0x1,          // File requires secure deletion (not used)
    UNRM_FL         = 0x2,          // File should be preserved, should undeletion be desired (not used)
    COMPR_FL        = 0x4,          // File is compressed (not really implemented)
    SYNC_FL         = 0x8,          // All writes to the file must be synchronous
    IMMUTABLE_FL    = 0x10,         // File is immutable
    APPEND_FL       = 0x20,         // File can only be appended
    NODUMP_FL       = 0x40,         // The dump utility should not dump this file (not used in kernel)
    NOATIME_FL      = 0x80,         // Do not update access time
    DIRTY_FL        = 0x100,        // Dirty compressed file (not used)
    COMPRBLK_FL     = 0x200,        // File has one or  more compressed blocks (not used)
    NOCOMPR_FL      = 0x400,        // Don't compress (not used)
    ENCRYPT_FL      = 0x800,        // Encrypted inode
    INDEX_FL        = 0x1000,       // Hashed directory entries
    IMAGIC_FL       = 0x2000,       // AFS magic directory
    JOURNAL_DATA_FL = 0x4000,       // File data must always be written through journal
    NOTAIL_FL       = 0x8000,       // Tail shouldn't merge (not used by ext4)
    DIRSYNC_FL      = 0x10000,      // All directory entry data must be written synchronously
    TOPDIR_FL       = 0x20000,      // Top of directory hierarchy
    HUGE_FILE_FL    = 0x40000,      // This is a huge file
    EXTENTS_FL      = 0x80000,      // Inode uses extents
    EA_INODE_FL     = 0x200000,     // Inode stores a large extended attribute in its data blocks
    EOFBLOCKS_FL    = 0x400000,     // The file has blocks past EOF (deprecated)
    SNAP_FL         = 0x1000000,    // Inode is a snapshot (not officially supported)
    SNAP_DELETED_FL = 0x4000000,    // Snapshot is being deleted (not officially supported)
    SNAP_SHRUNK_FL  = 0x8000000,    // Snapshot shrink has completed (not officially supported)
    INLINE_DATA_FL  = 0x10000000,   // Inode has inline data
    PROJINHERIT_FL  = 0x20000000    // Create children with the same project ID
};

#define S_ISFIFO(m) ((m & 0xF000) == S_IFIFO)
#define S_ISCHR(m)  ((m & 0xF000) == S_IFCHR)
#define S_ISDIR(m)  ((m & 0xF000) == S_IFDIR)
#define S_ISBLK(m)  ((m & 0xF000) == S_IFBLK)
#define S_ISREG(m)  ((m & 0xF000) == S_IFREG)
#define S_ISLNK(m)  ((m & 0xF000) == S_IFLNK)
#define S_ISSOCK(m) ((m & 0xF000) == S_IFSOCK)

struct Inode
{
    uint16_t Mode;              // See the enum above
    uint16_t UID;
    uint32_t SizeLo;
    uint32_t ATime;             // Access time
    uint32_t CTime;             // Inode change time
    uint32_t MTime;             // Last data modification time
    uint32_t DTime;             // Deletion time
    uint16_t GID;
    uint16_t LinksCount;        // Note: If DIR_NLINK is set, we support more than 64998 subdirs by setting this field to "1"
    uint32_t BlocksLo;
    uint32_t Flags;             // See the enum above
    uint8_t  OSD1[4];           // OS-specific data
    uint32_t Blocks[15];
    uint32_t Generation;        // File version (for NFS)
    uint32_t FileACLLo;
    uint32_t SizeHi;
    uint32_t ObsoleteFrag;      // (Obsolete) fragment address
    struct
    {
        uint16_t BlocksHi;      // Upper 16-bits of the block count
        uint8_t  Reserved[10];
    } OSD2;
    uint16_t InodeSize;         // Size of this inode - 128
    uint16_t ChecksumHi;
    uint32_t CTimeExtra;        // Change time extra bits
    uint32_t MTimeExtra;        // Modify time extra bits
    uint32_t ATimeExtra;        // Access time extra bits
    uint32_t CRTime;            // Creation time
    uint32_t CRTimeExtra;       // Creation time extra bits
    uint32_t VersionHi;         // Upper 32bits of version
    uint32_t ProjID;            // Project ID.

    /*
     * Conventient getters / setters.
     */

    inline uint16_t GetMode() const
    {
        return le16toh(Mode);
    }

    inline uint16_t GetUID() const
    {
        return le16toh(UID);
    }

    inline uint16_t GetGID() const
    {
        return le16toh(GID);
    }

    inline uint32_t GetATime() const
    {
        return le32toh(ATime);
    }

    inline uint32_t GetATimeExtra() const
    {
        return le32toh(ATimeExtra);
    }

    inline uint32_t GetCTime() const
    {
        return le32toh(CTime);
    }

    inline uint32_t GetCTimeExtra() const
    {
        return le32toh(CTimeExtra);
    }

    inline uint32_t GetMTime() const
    {
        return le32toh(MTime);
    }

    inline uint32_t GetMTimeExtra() const
    {
        return le32toh(MTimeExtra);
    }

    inline uint32_t GetCRTime() const
    {
        return le32toh(CRTime);
    }

    inline uint32_t GetCRTimeExtra() const
    {
        return le32toh(CRTimeExtra);
    }

    inline uint16_t GetLinksCount() const
    {
        return le16toh(LinksCount);
    }

    inline uint32_t GetBlocksLo() const
    {
        return le32toh(BlocksLo);
    }

    inline uint32_t GetBlocksHi() const
    {
        return le32toh(OSD2.BlocksHi);
    }

    inline uint32_t GetSizeLo() const
    {
        return le32toh(SizeLo);
    }

    inline uint32_t GetSizeHi() const
    {
        return le32toh(SizeHi);
    }

    inline uint32_t GetFlags() const
    {
        return le32toh(Flags);
    }
};

}