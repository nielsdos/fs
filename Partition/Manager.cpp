/*
 * MIT License
 *
 * Copyright (c) 2018 The Copper authors and contributors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <cstdio>
#include <FileSystem/Partition/Manager.hpp>
#include <FileSystem/Partition/PartitionNode.hpp>
#include <FileSystem/Partition/MBR.hpp>
#include <FileSystem/Partition/GPT.hpp>
#include <FileSystem/VFS.hpp>

namespace FileSystem::Partition::Manager
{

/**
 * Add partitions of a partition table
 * @param shortName The name of the block device
 * @param blockDevice The block device
 * @param table The partition table
 */
static void AddPartitions(const char* shortName, Node* blockDevice, const IPartitionTable& table)
{
    size_t shortNameLen = strlen(shortName);
    const auto partitions = table.GetPartitions();

    // No more than UINT16_T_MAX partitions per block device
    for(const auto& p : *partitions)
    {
        /*
         * Example: ata1p4
         * UINT16_T_MAX is 65536, 5 characters are needed for that number
         * Note: the use of sprintf is safe (see above)
         */
        char* name = new char[shortNameLen + 1 + 5 + 1];
        if(!name)
            continue;

        sprintf(name, "%sp%u", shortName, p.Num);

        PartitionNode* node = new PartitionNode(p, blockDevice);
        if(!node)
        {
            delete[] name;
            continue;
        }

        if(AddDevice(name, node) != ContainerError::OK)
        {
            delete[] name;
            delete node;
        }
    }
}

bool ProcessPartitions(const char* name)
{
    Node* blockDevice = Open(name);
    if(blockDevice == nullptr)
        return false;

    /*
     * Both GPT and MBR have an MBR header.
     * GPT has a protective MBR, which should have a single partition with GPT type.
     * Which means we need to parse the device's MBR anyway.
     */
    MBR mbr(blockDevice);
    if(!mbr.IsValid())
    {
        blockDevice->Close();
        return false;
    }

    const char* shortName = strrchr(name, '/') + 1;

    // Verify if GPT is possible on this block device
    if(GPT::IsPossible(mbr))
    {
        GPT gpt(blockDevice);
        if(!gpt.IsValid())
        {
            blockDevice->Close();
            return false;
        }

        AddPartitions(shortName, blockDevice, gpt);
    }
    // Just a MBR
    else
    {
        AddPartitions(shortName, blockDevice, mbr);
    }

    blockDevice->Close();
    return true;
}

}