/*
 * MIT License
 *
 * Copyright (c) 2018 The Copper authors and contributors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <errno.h>
#include <FileSystem/VFS.hpp>
#include <FileSystem/IFileSystem.hpp>
#include <FileSystem/Partition/Manager.hpp>
#include <FileSystem/Partition/PartitionNode.hpp>

namespace FileSystem::Partition
{

PartitionNode::PartitionNode(const Partition& part, Node* blockDevice)
    : _blockDevice(blockDevice), _start(part.Start), _length(part.Length)
{
    _type = DT_BLK;
    _devNumber = MakeDev(GetMajorFromDev(blockDevice->GetDevNumber()), part.Num);
}

ssize_t PartitionNode::PRead(void* buffer, size_t count, off_t offset)
{
    // "+" overflow
    if((size_t)offset > OFF_T_MAX - count - _start)
    {
        errno = EOVERFLOW;
        return -1;
    }

    // Out of bounds
    if(offset > _length)
    {
        return 0;
    }
    else if(count > (size_t)_length - offset)
    {
        count = _length - offset;
    }

    return _blockDevice->PRead(buffer, count, _start + offset);
}

ssize_t PartitionNode::PWrite(const void* buffer, size_t count, off_t offset)
{
    // "+" overflow
    if((size_t)offset > OFF_T_MAX - count - _start)
    {
        errno = EOVERFLOW;
        return -1;
    }

    // Out of bounds
    if(offset > _length)
    {
        return 0;
    }
    else if(count > (size_t)_length - offset)
    {
        count = _length - offset;
    }

    return _blockDevice->PWrite(buffer, count, _start + offset);
}

uint32_t PartitionNode::GetBlockSize() const
{
    return _blockDevice->GetBlockSize();
}

off_t PartitionNode::GetSize() const
{
    return _length;
}

dev_t PartitionNode::GetDevNumber() const
{
    return _devNumber;
}

const IFileSystem* PartitionNode::GetFS() const
{
    return _FS;
}

void PartitionNode::SetFS(IFileSystem* fs)
{
    _FS = fs;
}

}