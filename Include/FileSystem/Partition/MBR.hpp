/*
 * MIT License
 *
 * Copyright (c) 2018 The Copper authors and contributors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <stdint.h>
#include <FileSystem/Partition/IPartitionTable.hpp>
#include <FileSystem/Partition/Manager.hpp>

namespace FileSystem::Partition
{

struct MBRHeader;
struct MBRPartition;

class MBR : public IPartitionTable
{
public:
    /**
     * Parses a MBR
     * @param blockDevice The block device
     */
    MBR(Node* blockDevice);

    /**
     * Checks if a table is valid
     * @return True if the table is valid
     */
    bool IsTableValid(const MBRHeader& mbr) const;

    /**
     * Parses a partition
     * @param mbr Reference to the MBR structure
     * @param n The partition number
     * @param offset The offset in the disk
     */
    void ParsePartition(const MBRHeader& mbr, int n, off_t offset = 0);

    /**
     * Checks if a partition is extended
     * @return True if the partition is extended
     */
    bool IsExtendedPartition(const MBRPartition* part) const;

    /**
     * Checks if a partition is an EFI system partition (which means that this is a legacy MBR followed by GPT)
     * @param part The partition
     * @return True if the partition is an EFI system partition
     */
    bool IsEFIPartition(const MBRPartition* part) const;

    /**
     * Checks if this MBR has contains an EFI system partition
     * @return True if the MBR contains an EFI system partition
     */
    inline bool HasEFISystemPartition() const
    {
        return _hasEFISystemPartition;
    }

private:
    bool     _hasEFISystemPartition = false;
    uint16_t _currentID = 1;
};

}