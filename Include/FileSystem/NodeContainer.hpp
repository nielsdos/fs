/*
 * MIT License
 *
 * Copyright (c) 2018 The Copper authors and contributors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <FileSystem/IFileSystem.hpp>
#include <FileSystem/Node.hpp>
#include <DataStructure/HashMap.hpp>
#include <Sync/Mutex.hpp>

namespace FileSystem
{

enum class ContainerError
{
    OK,
    NO_MEM,
    INVALID_NAME,
    DUPLICATE_ENTRY,
    NO_ENTRY,
    NO_ROOT
};

/*
 * In memory hashmap-based filesystem for containing nodes.
 */
class NodeContainer : public IFileSystem
{
private:
    /*
     * Contains the hashmap, the root of the container.
     */
    class ContainerRoot : public Node
    {
    public:
        ContainerRoot(NodeContainer* fs);

        /**
         * Initializes the container
         * @return True if succesfully initialized
         */
        bool Init();

        Node* Open(const char* name);
        off_t GetSize() const;
        ssize_t ReadDirEntries(DirEnt* dirents, size_t count, off_t& offset);
        const IFileSystem* GetFS() const;

        /**
         * Adds a node
         * @param name The name
         * @param node The node
         * @return The error status
         */
        ContainerError Add(const char* name, Node* node);

        /**
         * Removes a node
         * @param name The name
         * @return True if success
         */
        bool Remove(const char* name);

        /**
         * Syncs the nodes inside the container
         * @return True if succesful
         */
        bool Sync();

    private:
        DataStructure::HashMap<const char*, Node*, DataStructure::StringHasher> _nodes = DataStructure::HashMap<const char*, Node*, DataStructure::StringHasher>(8);
        Sync::Mutex _lock;
        NodeContainer* _FS;
        ino_t _currentIno = 1;
    };

public:
    NodeContainer();

    inline Node* GetRoot()
    {
        _root.IncRefCount();
        return &_root;
    }

    bool Sync();

    dev_t GetDevNumber() const;

    /**
     * Initializes the container
     * @return True if succesfully initialized
     */
    inline bool Init()
    {
        return _root.Init();
    }

    /**
     * Adds a node
     * @param name The name
     * @param entry The entry
     * @return The error status
     */
    ContainerError Add(const char* name, Node* node);

    /**
     * Removes a node
     * @param name The name
     * @return The error status
     */
    ContainerError Remove(const char* name);

private:
    ContainerRoot _root = ContainerRoot(this);
    dev_t _deviceNumber;
};

}