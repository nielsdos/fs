/*
 * MIT License
 *
 * Copyright (c) 2018 The Copper authors and contributors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <stdint.h>
#include <FileSystem/Partition/IPartitionTable.hpp>

namespace FileSystem
{

class Node;

namespace Partition
{

class MBR;

class GPT : public IPartitionTable
{
public:
    /**
     * Parses a GPT
     * @param blockDevice The block device
     */
    GPT(Node* blockDevice);

    /**
     * Checks if it is possible that the given MBR is a protective MBR for GPT
     * @param mbr The MBR
     * @return If a GPT is possible
     */
    static bool IsPossible(MBR& mbr);

private:
    /**
     * Parses the partitions
     */
    bool ParsePartitions();

    uint64_t _firstLBA;
    uint64_t _lastLBA;
    uint64_t _entriesLBA;
    uint32_t _partitionCount;
    uint32_t _CRC32Entries;
    uint32_t _entrySize;
    uint16_t _currentID = 1;
};

}

}