/*
 * MIT License
 *
 * Copyright (c) 2018 The Copper authors and contributors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <FileSystem/DirEnt.hpp>
#include <FileSystem/Node.hpp>
#include <Sync/Mutex.hpp>

namespace FileSystem
{

// File whence
// TODO: enable this code/fix this uglyness
#ifndef SEEK_SET
constexpr int SEEK_SET = 0;
constexpr int SEEK_CUR = 1;
constexpr int SEEK_END = 2;
#endif

struct Stat;

class Descriptor
{
public:
    /**
     * Creates a new descriptor
     * @param node The node
     */
    Descriptor(Node* node);
    ~Descriptor();

    /**
     * Open a node
     * @param name The name of the node
     * @return The node
     */
    Node* Open(const char* name);

    /**
     * Closes a descriptor
     */
    void Close();

    /**
     * Gets the size of this node
     * @return The size
     */
    inline off_t GetSize() const
    {
        return _node->GetSize();
    }

    /**
     * Obtain information of this node in a buffer
     * @param buf The buffer
     */
    inline void GetStat(Stat* buf) const
    {
        _node->GetStat(buf);
    }

    /**
     * Change mode of a file
     * @param mode The new mode flags
     * @return True if succesful
     */
    inline bool ChMod(mode_t mode)
    {
        return _node->ChMod(mode);
    }

    /**
     * Move the read/write file offset
     * @param offset The file offset (behaviour controlled by whence)
     * @param whence The direction
     * @return The new offset
     */
    off_t Seek(off_t offset, int whence);

    /**
     * Reads data from the node
     * @param buffer The buffer to put the data into
     * @param count The size of the buffer
     * @return The amount of bytes read, -1 if error
     */
    ssize_t Read(void* buffer, size_t count);

    /**
     * Writes data to the node
     * @param buffer The buffer to read the data from
     * @param count The size of the buffer
     * @return The amount of bytes written, -1 if error
     */
    ssize_t Write(const void* buffer, size_t count);

    /**
     * Reads the directory entries
     * @param dirents The buffer where to put the directory entries into
     * @param amount The amount of directory entries to read
     * @return The amount of bytes read
     */
    ssize_t ReadDirEntries(DirEnt* dirents, size_t amount);

    /**
     * Gets the block size
     * @return The block size
     */
    inline uint32_t GetBlockSize() const
    {
        return _node->GetBlockSize();
    }

    /**
     * Returns the inode number
     * @return Inode number
     */
    inline ino_t GetIno() const
    {
        return _node->GetIno();
    }

    /**
     * Returns the file type
     * @return File type
     */
    inline unsigned char GetType() const
    {
        return _node->GetType();
    }

    /**
     * Returns the current offset
     * @return The current offset
     */
    inline off_t Tell() const
    {
        return _offset;
    }

private:
    off_t _offset = 0;
    Sync::Mutex _offsetLock;
    Node* _node;
};

}