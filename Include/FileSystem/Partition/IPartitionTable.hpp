/*
 * MIT License
 *
 * Copyright (c) 2018 The Copper authors and contributors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <DataStructure/ArrayList.hpp>
#include <FileSystem/Partition/Manager.hpp>

namespace FileSystem
{

class Node;

namespace Partition
{

class IPartitionTable
{
public:
    /**
     * Initializes the partition table
     * @return True if succesfully initialized
     */
    inline bool Init()
    {
        return _partitions.Init();
    }

    /**
     * Checks if the GPT is valid
     * @return True if the GPT is valid
     */
    inline bool IsValid() const
    {
        return _valid;
    }

    /**
     * Gets the partition list
     * @return The partitions
     */
    inline const DataStructure::ArrayList<Partition>* GetPartitions() const
    {
        return &_partitions;
    }

    /**
     * Gets the partition count
     * @return The partition count
     */
    inline size_t GetPartitionCount() const
    {
        return _partitions.GetSize();
    }

protected:
    /**
     * Creates a new IPartitionTable
     * @param blockDevice The block device
     */
    IPartitionTable(Node* blockDevice);

    /**
     * Adds a partition
     * @param partition The partition
     */
    void AddPartition(Partition& partition);

    Node* _blockDevice;
    bool _valid = false;
    DataStructure::ArrayList<Partition> _partitions = DataStructure::ArrayList<Partition>(4);
};

}

}