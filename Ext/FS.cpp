/*
 * MIT License
 *
 * Copyright (c) 2018 The Copper authors and contributors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <assert.h>
#include <malloc.h>
#include <FileSystem/Ext/FS.hpp>
#include <FileSystem/Ext/Block.hpp>
#include <FileSystem/Ext/Hash.hpp>
#include <FileSystem/Ext/Features.hpp>
#include <FileSystem/Ext/BlockGroup.hpp>
#include <FileSystem/Ext/ExtNode.hpp>
#include <FileSystem/VFS.hpp>
#include <Time/Clock.hpp>
#include <Utils/Math.hpp>
#include <Utils/Logger.hpp>
#include<iostream>
namespace FileSystem::Ext
{

FS::~FS()
{
    // Not initialized properly
    if(!_blockDevice)
        return;

    // Still mounted, but we're in a destructor???
    assert(!_mountedOn);

    for(uint32_t i = 0; i < _BGDescriptors; i++)
    {
        if(_blockGroups[i])
        {
            _blockGroups[i]->Sync();
            delete _blockGroups[i];
        }
    }

    delete[] _blockGroups;
    delete _root;

    _blockDevice->Close();
}

Node* FS::GetRoot()
{
    assert(_root);
    _root->IncRefCount();
    return _root;
}

bool FS::Sync() const
{
    std::cout << "ext fs sync"<<std::endl;
    for(uint32_t i = 0; i < _BGDescriptors; i++)
    {
        if(_blockGroups[i])
            _blockGroups[i]->Sync();
    }

    return true;
}

dev_t FS::GetDevNumber() const
{
    return _blockDevice->GetDevNumber();
}

uint32_t FS::GetBlockSize() const
{
    return _SB.GetBlockSize();
}

bool FS::CheckCompatibility()
{
    Logger::Debug("EXT: Compat features: 0x%x, RO compat features: 0x%x, Incompat features: 0x%x",
        _SB.GetFeatureCompat(), _SB.GetFeatureROCompat(), _SB.GetFeatureIncompat());

    // Kernel must stop if incompatible flag is set
    if(_SB.GetFeatureIncompat() & ~INCOMPAT_FLAGS)
    {
        Logger::Warn("EXT: Cannot mount volume: incompatible flags: 0x%x",
            _SB.GetFeatureIncompat() & ~INCOMPAT_FLAGS);
        // TODO: re-enable
        //return false;
    }

    /* Kernel must mount read-only if ro flag is set*/
    if(_SB.GetFeatureROCompat() & ~RO_COMPAT_FLAGS)
    {
        Logger::Warn("EXT: Mounting volume read-only because of unknown read-only flags: 0x%x",
            _SB.GetFeatureROCompat() & ~RO_COMPAT_FLAGS);
        _isReadOnly = true;
    }

    // FS has read-only flag set
    if(HasROCompat(ROCompatFlag::READ_ONLY))
    {
        Logger::Info("EXT: Volume has read-only flag set in superblock");
        _isReadOnly = true;
    }

    // Validate hashing algorithm stuff
    if(HasROCompat(ROCompatFlag::BTREE_DIR) || HasCompat(CompatFlag::DIR_INDEX))
    {
        if(_SB.GetHashVersion() != DirHash::LEGACY &&
           _SB.GetHashVersion() != DirHash::HALF_MD4 &&
           _SB.GetHashVersion() != DirHash::TEA)
        {
            Logger::Warn("EXT: Invalid tree hashing algorithm: %d", _SB.GetHashVersion());
            return false;
        }
    }

    // Not cleanly unmounted last time?
    if((_SB.GetState() & FSState::CLEAN) == 0)
        Logger::Warn("EXT: Volume wasn't unmounted cleanly");

    return true;
}

MountError FS::Mount(const char* destination, const char* blockDevName, MountFlag flags)
{
    if(!_blockCache.Init())
        return MountError::NO_MEM;

    _blockDevice = Open(blockDevName);
    if(!_blockDevice)
        return MountError::INVALID_BLOCKDEV;

    if(_blockDevice->PRead(&_SB, sizeof(SuperBlock), 1024) != sizeof(SuperBlock))
    {
        _blockDevice->Close();
        return MountError::INVALID_FS;
    }

    if(!_SB.IsValid())
    {
        Logger::Warn("EXT: Superblock is invalid");
        return MountError::INVALID_FS;
    }

    // No EXT_DYNAMIC_REV
    if(_SB.GetRevisionLevel() == 0)
    {
        Logger::Warn("EXT2: Filesystem too old: revision level is 0");
        return MountError::INCOMPAT_FS;
    }

    // Process mount flags
    _isReadOnly = (flags & MountFlag::READ_ONLY);
    if(_isReadOnly)
        Logger::Info("EXT: Mount has read-only flag set");

    if(!CheckCompatibility())
        return MountError::INCOMPAT_FS;

    // Prepare data
    uint64_t blocksCount = _SB.GetBlocksCountLo() - _SB.GetFirstDataBlock();
    uint32_t blocksPerGroup = _SB.GetBlocksPerGroup();
    _is64 = HasIncompat(IncompatFlag::BLOCK64);
    if(_is64)
    {
        _BGDescriptorSize = _SB.GetDescriptorSize();
        if(_BGDescriptorSize < 64)
            return MountError::INVALID_FS;

        blocksCount |= (uint64_t)_SB.GetBlocksCountHi() << 32;
    }

    _BGDescriptors = Utils::Math::DivCeil(blocksCount, (uint64_t)blocksPerGroup);
    _blockGroups = new BlockGroup*[_BGDescriptors];
    if(!_blockGroups)
        return MountError::NO_MEM;

    memset(_blockGroups, 0, sizeof(BlockGroup*) * _BGDescriptors);

    // Get root so we can mount
    _root = GetNodeFromInode(2);
    if(!_root)
    {
        Logger::Warn("EXT: Couldn't get root?");
        return MountError::INVALID_FS;
    }

    // Actually does the mount: adds the volume
    ContainerError error = AddMountPoint(destination);
    if(error != ContainerError::OK)
    {
        _root->Close();
        return MountError::NOT_MOUNTED;
    }

    // Update superblock
    if(!_isReadOnly)
    {
        _SB.SetMountTime((uint32_t)Time::GetEpoch());
        _SB.SetMountCount(_SB.GetMountCount() + 1);
        strlcpy(_SB.LastMounted, _mountedOn, sizeof(_SB.LastMounted));
        _SB.SetState((_SB.GetState() & ~FSState::CLEAN) | FSState::ERRORS);
        _blockDevice->PWrite(&_SB, sizeof(SuperBlock), 1024);
    }

    return MountError::OK;
}

bool FS::UnMount()
{
    if(!MountableFileSystem::UnMount())
        return false;

    // Close reference we got from the GetRoot() call
    _root->DecRefCount();

    Sync();

    // Update superblock
    if(!_isReadOnly)
    {
        _SB.SetState((_SB.GetState() & ~FSState::ERRORS) | FSState::CLEAN);
        _blockDevice->PWrite(&_SB, sizeof(SuperBlock), 1024);
    }

    return true;
}

ExtNode* FS::GetNodeFromInode(uint32_t id)
{
    uint32_t group = (id - 1) / GetInodesPerGroup();
    BlockGroup* bg = GetBlockGroup(group);
    if(bg)
        return bg->GetNodeFromInode(id);

    return nullptr;
}

BlockGroup* FS::GetBlockGroup(uint32_t id)
{
    if(id >= _BGDescriptors)
        return nullptr;

    if(!_blockGroups[id])
    {
        BlockGroupDesc* bgd = (BlockGroupDesc*)malloc(_BGDescriptorSize);
        if(!bgd)
            return nullptr;

        uint32_t blockSize = GetBlockSize();
        uint8_t* buffer = new uint8_t[blockSize];
        if(!buffer)
        {
            delete bgd;
            return nullptr;
        }

        uint32_t block = 1 + _SB.GetFirstDataBlock() + (id * _BGDescriptorSize / blockSize);
        uint32_t off   = (id * _BGDescriptorSize) % blockSize;

        if(ReadBlockDevice(buffer, block, 1))
        {
            memcpy(bgd, buffer + off, _BGDescriptorSize);
            // If this allocation fails, this will be null, no extra null check needed
            _blockGroups[id] = new BlockGroup(this, id, bgd);
        }

        delete[] buffer;
    }

    return _blockGroups[id];
}

Block* FS::GetBlock(uint64_t id)
{
    // TODO: oldest blocks with refcount==0 moeten weg, linked list ofzo bijhouden?
    //       is het dan niet beter van hier een custom hashmap+linked list te bouwen?
    Block* block;
    if(!_blockCache.Get(id, block))
    {
        uint8_t* data = new uint8_t[GetBlockSize()];
        if(!data)
            return nullptr;

        if(!ReadBlockDevice(data, id, 1))
        {
            delete[] data;
            return nullptr;
        }

        block = new Block(this, id, data);
        if(!block)
        {
            delete[] data;
            return nullptr;
        }

        _blockCache.Add(id, block);
    }

    return block;
}

bool FS::ReadBlockDevice(void* buffer, uint64_t blockNum, uint32_t blockCount) const
{
    uint32_t blockSize = GetBlockSize();
    ssize_t read = _blockDevice->PRead(buffer, blockCount * (size_t)blockSize, blockNum * (off_t)blockSize);
    return (read > 0);
}

bool FS::WriteBlockDevice(const void* buffer, uint64_t blockNum, uint32_t blockCount) const
{
    assert(!_isReadOnly);
    uint32_t blockSize = GetBlockSize();
    ssize_t written = _blockDevice->PWrite(buffer, blockCount * (size_t)blockSize, blockNum * (off_t)blockSize);
    return (written > 0);
}

}