/*
 * MIT License
 *
 * Copyright (c) 2018 The Copper authors and contributors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <cassert>
#include <errno.h>
#include <FileSystem/Descriptor.hpp>
#include <FileSystem/Node.hpp>
#include <FileSystem/Stat.hpp>
#include <Sync/ScopedLock.hpp>

namespace FileSystem
{

Descriptor::Descriptor(Node* node)
    : _node(node)
{
    assert(node);
}

Descriptor::~Descriptor()
{
    Close();
}

Node* Descriptor::Open(const char* name)
{
    if(_node->GetType() != DT_DIR)
    {
        errno = ENOTDIR;
        return nullptr;
    }

    return _node->Open(name);
}

void Descriptor::Close()
{
    assert(_node);
    _node->Close();
    _node = nullptr;
}

off_t Descriptor::Seek(off_t offset, int whence)
{
    Sync::ScopedLock lock(_offsetLock);

    // Final offset is calculated as offset + relative offset
    off_t relative;
    if(whence == SEEK_SET)
        relative = 0;
    else if(whence == SEEK_CUR)
        relative = _offset;
    else if(whence == SEEK_END)
        relative = GetSize();
    else
    {
        errno = EINVAL;
        return -1;
    }

    // Overflow/Underflow
    if(offset > OFF_T_MAX - relative || relative + offset < 0)
    {
        errno = EOVERFLOW;
        return -1;
    }

    _offset = _node->Seek(_offset, relative + offset);
    return _offset;
}

ssize_t Descriptor::Read(void* buffer, size_t count)
{
    if(GetType() == DT_DIR)
    {
        errno = EISDIR;
        return -1;
    }

    Sync::ScopedLock lock(_offsetLock);

    // Overflow
    if(_offset > OFF_T_MAX - (off_t)count)
    {
        errno = EOVERFLOW;
        return -1;
    }

    ssize_t read = _node->PRead(buffer, count, _offset);
    if(read >= 0)
    {
        _offset += read;
        return read;
    }

    return -1;
}

ssize_t Descriptor::Write(const void* buffer, size_t count)
{
    if(GetType() == DT_DIR)
    {
        errno = EISDIR;
        return -1;
    }

    Sync::ScopedLock lock(_offsetLock);

    // Overflow
    if(_offset > OFF_T_MAX - (off_t)count)
    {
        errno = EOVERFLOW;
        return -1;
    }

    ssize_t written = _node->PWrite(buffer, count, _offset);
    if(written >= 0)
    {
        _offset += written;
        return written;
    }

    return -1;
}

ssize_t Descriptor::ReadDirEntries(DirEnt* dirents, size_t amount)
{
    /*
     * In directories, the offset is somewhat special, it doesn't work like how it works with files:
     * If you read a file, you know how much bytes to add to the offset because you know how much bytes you read.
     * The "read" bytes of ReadDirEntries doesn't mean how much bytes are read on the FS, but how much bytes
     * are read into the buffer. This is different because there might be gaps etc. in the FS directory structure.
     */
    _offsetLock.Lock();
    ssize_t read = _node->ReadDirEntries(dirents, amount, _offset);
    _offsetLock.Unlock();
    return read;
}

}