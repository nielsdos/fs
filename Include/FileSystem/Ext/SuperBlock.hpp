/*
 * MIT License
 *
 * Copyright (c) 2018 The Copper authors and contributors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <endian.h>
#include <stdint.h>

namespace FileSystem::Ext
{

typedef enum
{
    CLEAN = 0x1,
    ERRORS = 0x2,
    RECOVERING = 0x4
} FSState;

/*
 * All fields are little endian.
 */
struct SuperBlock
{
    uint32_t InodesCount;
    uint32_t BlocksCountLo;
    uint32_t RBlocksCountLo;        // This number of blocks can only be allocated by super-user
    uint32_t FreeBlocksCountLo;
    uint32_t FreeInodesCount;
    uint32_t FirstDataBlock;
    uint32_t LogBlockSize;          // Block size = 2^(10 + LogBlockSize)
    uint32_t LogClusterSize;        // Cluster size = 2^LogClusterSize
    uint32_t BlocksPerGroup;
    uint32_t ClustersPerGroup;
    uint32_t InodesPerGroup;
    uint32_t MountTime;
    uint32_t WriteTime;
    uint16_t MountCount;            // Number of mounts since last fsck
    uint16_t MaxMountCount;         // Maximum number of mounts until a fsck
    uint16_t Magic;                 // Must be 0xEF53 in little endian
    uint16_t State;
    uint16_t Errors;
    uint16_t MinorRevision;
    uint32_t LastCheck;
    uint32_t CheckInterval;
    uint32_t CreatorOS;
    uint32_t RevisionLevel;
    uint16_t DefaultResUID;         // Default UID for reserved blocks
    uint16_t DefaultResGID;         // Default GID for reserved blocks
    uint32_t FirstInode;            // First non-reserved inode
    uint16_t InodeSize;
    uint16_t BlockGroupNr;          // Block group number of this superblock
    uint32_t FeatureCompat;         // Compatible features
    uint32_t FeatureIncompat;       // Incompatible features, if we don't understand one of these, we should stop
    uint32_t FeatureROCompat;       // If the kernel doesn't understand one of these, we should mount read-only
    uint8_t  UUID[16];
    char     VolumeName[16];
    char     LastMounted[64];       // Directory where the FS was last mounted
    uint32_t AlgoUsageBitmap;       // For compression
    uint8_t  PreallocBlocks;        // Number of blocks to try to preallocate for files
    uint8_t  PreallocDirBlocks;     // Number of blocks to try to preallocate for directories
    uint16_t ReservedGDTBlocks;
    uint8_t  JournalUUID[16];
    uint32_t JournalInode;          // Inode number of journal file
    uint32_t JournalDevice;         // Device number of journal file
    uint32_t LastOrphan;            // Start of list of orphaned inodes to delete
    uint32_t HashSeed[4];           // HTree hash seed
    uint8_t  HashVersion;           // Default hash algorithm of directory hashes
    uint8_t  JournalBackupType;
    uint16_t DescriptorSize;
    uint32_t DefaultMountOpts;
    uint32_t FirstMetaBG;           // First metablock block group
    uint32_t CreationTime;
    uint32_t JournalBlocks[17];     // Backup copy of the journal inode's block array
    uint32_t BlocksCountHi;
    uint32_t RBlocksCountHi;
    uint32_t FreeBlocksCountHi;
    uint16_t MinExtraInodeSize;
    uint16_t WantExtraInodeSize;
    uint32_t Flags;
    uint16_t RaidStride;
    uint16_t MMPInterval;
    uint64_t MMPBlock;
    uint32_t RaidStripeWidth;
    uint8_t  LogGroupsPerFlex;      // Size of a flexible block group = 2^LogGroupsPerFlex
    uint8_t  ChecksumType;          // Should be 1 (CRC32C)
    uint16_t ReservedPadding;
    uint64_t KBytesWritten;         // Number of KiB written to this FS over its lifetime
    uint32_t SnapshotInodeNum;      // Inode number of active snapshot
    uint64_t SnapshotRBlocksCount;  // Number of blocks reserved for active snapshot's future use
    uint32_t SnapshotList;          // Inode number of the head of the on-disk snapshot list
    uint32_t ErrorCount;
    uint32_t FirstErrorTime;
    uint32_t FirstErrorInode;
    uint64_t FirstErrorBlock;
    uint8_t  FirstErrorFunc[32];
    uint32_t FirstErrorLine;
    uint32_t LastErrorTime;
    uint32_t LastErrorInode;
    uint32_t LastErrorLine;
    uint64_t LastErrorBlock;
    uint8_t  LastErrorFunc[32];
    uint8_t  MountOpts[64];         // ASCIIZ string of mount options
    uint32_t UserQuoteInode;        // Inode number of user quote file
    uint32_t GroupQuoteInode;       // Inode number of group quote file
    uint32_t OverheadBlocks;
    uint32_t BackupBGs[2];          // Block groups containing superblock backups
    uint8_t  EncryptAlgos[4];       // Encryption algorithms in use
    uint8_t  EncryptPWSalt[16];     // Salt for the string2key algorithm for encryption
    uint32_t LostFoundInode;        // Inode number of lost+found
    uint32_t ProjectQuoteInode;     // Inode that tracks project quotas
    uint32_t ChecksumSeed;          // Checksum seed used for metadata csum calculations
    uint32_t Reserved[98];
    uint32_t Checksum;

    /*
     * Conventient getters / setters.
     */

    inline uint16_t GetMountCount() const
    {
        return le16toh(MountCount);
    }

    inline void SetMountCount(uint16_t count)
    {
        MountCount = htole16(count);
    }

    inline void SetMountTime(uint32_t time)
    {
        MountTime = htole32(time);
    }

    inline uint32_t GetFirstDataBlock() const
    {
        return le32toh(FirstDataBlock);
    }

    inline uint32_t GetBlockSize() const
    {
        return 1024U << le32toh(LogBlockSize);
    }

    inline uint32_t GetBlocksCountLo() const
    {
        return le32toh(BlocksCountLo);
    }

    inline uint32_t GetBlocksCountHi() const
    {
        return le32toh(BlocksCountHi);
    }

    inline uint32_t GetBlocksPerGroup() const
    {
        return le32toh(BlocksPerGroup);
    }

    inline uint32_t GetInodesPerGroup() const
    {
        return le32toh(InodesPerGroup);
    }

    inline uint16_t GetInodeSize() const
    {
        return le16toh(InodeSize);
    }

    inline uint16_t GetRevisionLevel() const
    {
        return le16toh(RevisionLevel);
    }

    inline uint32_t GetFeatureCompat() const
    {
        return le32toh(FeatureCompat);
    }

    inline uint32_t GetFeatureROCompat() const
    {
        return le32toh(FeatureROCompat);
    }

    inline uint32_t GetFeatureIncompat() const
    {
        return le32toh(FeatureIncompat);
    }

    inline uint8_t GetHashVersion() const
    {
        return HashVersion;
    }

    inline uint16_t GetDescriptorSize() const
    {
        return le16toh(DescriptorSize);
    }

    inline uint16_t GetState() const
    {
        return le16toh(State);
    }

    inline void SetState(uint16_t state)
    {
        State = htole16(state);
    }

    /**
     * Validates the superblock
     * @return True if the superblock is valid
     */
    inline bool IsValid() const
    {
        #if __BYTE_ORDER == __LITTLE_ENDIAN
        const uint16_t expectedMagic = 0xEF53;
        #else
        const uint16_t expectedMagic = 0x53EF;
        #endif

        if(Magic != expectedMagic)
            return false;
        if(GetInodeSize() > GetBlockSize())
            return false;
        if(GetBlocksPerGroup() != GetBlockSize() * 8)
            return false;

        return true;
    }
};

}