/*
 * MIT License
 *
 * Copyright (c) 2018 The Copper authors and contributors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <FileSystem/Partition/IPartitionTable.hpp>
#include <Panic.hpp>
#include <Utils/Logger.hpp>

namespace FileSystem::Partition
{

IPartitionTable::IPartitionTable(Node* blockDevice)
    : _blockDevice(blockDevice)
{
}

void IPartitionTable::AddPartition(Partition& partition)
{
    Logger::Debug("PartitionTable: Add partition [%ld - %ld]", partition.Start, partition.Start + partition.Length);

    // Check for overlaps with partitions that are already added
    for(const auto& part : _partitions)
    {
        /*
         * A logical partition will overlap with its associated extended partition
         * Note that if the extended had overlapped before with something else, it wouldn't have been added
         * Note also that extended partitions are added last
         */
        if(part.Type == PartitionType::EXTENDED ||
            (part.Type == PartitionType::LOGICAL && partition.Type == PartitionType::EXTENDED))
            continue;

        // Overlap check, don't add overlapping partitions
        if((partition.Start > part.Start &&
            partition.Start < part.Start + part.Length) ||
           (partition.Start < part.Start &&
            partition.Start + partition.Length > part.Start))
        {
            Logger::Warn("PartitionTable: Partition overlapping [%ld - %ld] with [%ld - %ld], skipping",
                partition.Start, partition.Start + partition.Length,
                part.Start, part.Start + part.Length);
            return;
        }
    }

    _partitions.Add(partition);
}

}