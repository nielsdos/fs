/*
 * MIT License
 *
 * Copyright (c) 2018 The Copper authors and contributors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <FileSystem/Ext/Block.hpp>
#include <Sync/ScopedLock.hpp>

namespace FileSystem::Ext
{

Block::Block(FS* fs, uint64_t block, uint8_t* buffer)
    : _FS(fs)/*, block(block)*/
{
    //
}

Block::~Block()
{
    Sync();
    //
}

void Block::Sync()
{
    Sync::ScopedLock lock(_writeLock);

    if(!_dirty)
        return;

    _dirty = false;
    //
}

void Block::BeginWrite()
{
    _writeLock.Lock();
}

void Block::EndWrite()
{
    _writeLock.Unlock();
    _dirty = true;
}

void Block::IncRefCount()
{
    _refLock.Lock();
    _refCount++;
    _refLock.Unlock();
}

void Block::DecRefCount()
{
    assert(_refCount > 0);
    _refLock.Lock();
    uint32_t old = _refCount--;
    _refLock.Unlock();
    if(old == 1)
    {
        /*...*/
    }
}

}