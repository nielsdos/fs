/*
 * MIT License
 *
 * Copyright (c) 2018 The Copper authors and contributors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

namespace FileSystem::Ext
{

/*
 * Compatible feature set flags.
 * Kernel is still allowed to read/write this FS even if it doesn't understand a flag.
 */
typedef enum
{
    DIR_PREALLOC     = 0x1,          // Block pre-allocation for new directories
    IMAGIC_INODES    = 0x2,          // Not documented
    HAS_JOURNAL      = 0x4,          // A journal exists
    EXT_ATTR         = 0x8,          // Extended inode attributes are present
    RESIZE_INODE     = 0x10,         // Non-standard inode size is used
    DIR_INDEX        = 0x20,         // Directories use a HTree
    LAZY_BG          = 0x40,         // Seems to have been for uninitialized block groups (?)
    EXCLUDE_INODE    = 0x80,         // Unused
    EXCLUDE_BITMAP   = 0x100,        // Unused/Not defined
    SPARSE_SUPER2    = 0x2000        // If set, BackupBGs points to the two block groups that contain backup superblocks
} CompatFlag;

/*
 * Incompatible feature set.
 * If the kernel doesn't understand one, it should stop.
 */
typedef enum
{
    COMPRESSION    = 0x1,          // Compression
    FILETYPE       = 0x2,          // Directory entries record the file type
    RECOVER        = 0x4,          // Filesystem needs recovery
    JOURNAL_DEV    = 0x8,          // Filesystem has a seperate journal device
    META_BG        = 0x10,         // Meta block groups
    EXTENTS        = 0x40,         // Files use extents instead of block mapping
    BLOCK64        = 0x80,         // Enable a fs size of 2^64 blocks
    MMP            = 0x100,        // Multiple mount protection
    FLEX_BG        = 0x200,        // Flexible block groups
    EA_INODE       = 0x400,        // Inodes can be used to storage large extended attribute values
    DIRDATA        = 0x1000,       // Data in directory entry (unused?)
    CSUM_SEED      = 0x2000,       // Metadata checksum seed is stored in the superblock
    LARGEDIR       = 0x4000,       // Large directory
    INLINE_DATA    = 0x8000,       // Data in inode
    ENCRYPT        = 0x10000       // Encrypted inodes are present
} IncompatFlag;

// List of features I understand
#define INCOMPAT_FLAGS (IncompatFlag::FILETYPE | \
                        IncompatFlag::BLOCK64)

/*
 * Readonly compatible feature set.
 * If the kernel doesn't understand one of these flags, it should mount read-only.
 */
typedef enum
{
    SPARSE_SUPER  = 0x1,          // Sparse superblocks
    LARGE_FILE    = 0x2,          // Filesystem stores files larger than 2 GiB
    BTREE_DIR     = 0x4,          // BTree directory
    HUGE_FILE     = 0x8,          // Filesystem has files whose size are represented in units of blocks
    GDT_CSUM      = 0x10,         // Group descriptors have checksums
    DIR_NLINK     = 0x20,         // A directory's link count will be set to 1 if it's incremented past 64,999
    EXTRA_ISIZE   = 0x40,         // Indicates that large inodes exist on the filesystem
    HAS_SNAPSHOT  = 0x80,         // This filesystem has a snapshot
    QUOTA         = 0x100,        // Quota
    BIGALLOC      = 0x200,        // File extents are tracked in units of clusters (of blocks) instead of blocks
    METADATA_CSUM = 0x400,        // Filesystem supports metadata checksumming
    REPLICA       = 0x800,        // Replica's (not used)
    READ_ONLY     = 0x1000,       // Read-only filesystem
    PROJECT       = 0x2000        // Filesystem tracks project quotas
} ROCompatFlag;

// List of features I understand
#define RO_COMPAT_FLAGS (ROCompatFlag::SPARSE_SUPER | \
                         ROCompatFlag::LARGE_FILE   | \
                         ROCompatFlag::HUGE_FILE    | \
                         ROCompatFlag::DIR_NLINK    | \
                         ROCompatFlag::EXTRA_ISIZE  | \
                         ROCompatFlag::READ_ONLY)

}