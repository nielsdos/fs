/*
 * MIT License
 *
 * Copyright (c) 2018 The Copper authors and contributors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <FileSystem/Node.hpp>
#include <FileSystem/Ext/Inode.hpp>
#include <FileSystem/Ext/ExtDirEnt.hpp>

struct timespec;

namespace FileSystem::Ext
{

class FS;

class ExtNode : public Node
{
public:
    /**
     * Creates a new node
     * @param fs The filesystem
     * @param inodeNum The inode number
     * @param inode The inode structure
     */
    ExtNode(FS* fs, uint32_t inodeNum, Inode* inode);
    ~ExtNode();

    off_t GetSize() const;
    void GetStat(struct Stat* buf) const;
    uint32_t GetBlockSize() const;
    const IFileSystem* GetFS() const;

protected:
    /**
     * Decodes time (extra bits etc)
     * @param ts The destination timespec
     * @param base The base time
     * @param extra The extra time bits
     */
    void DecodeTime(struct timespec& ts, uint32_t base, uint32_t extra) const;

    /**
     * Gets the access time
     * @param ts The destination timespec
     */
    void GetAccessTime(struct timespec& ts) const;

    /**
     * Gets the modified time
     * @param ts The destination timespec
     */
    void GetModifiedTime(struct timespec& ts) const;

    /**
     * Gets the change time
     * @param ts The destination timespec
     */
    void GetChangeTime(struct timespec& ts) const;

    /**
     * Gets the creation time
     * @param ts The destination timespec
     */
    void GetCreationTime(struct timespec& ts) const;

    /**
     * Finds an inode block number
     * @param block The block id
     * @return The block number
     */
    uint32_t FindBlock(uint32_t block) const;

    /**
     * Gets the type from the mode bits
     * @param mode The mode
     * @return The type
     */
    static unsigned char GetTypeFromMode(uint16_t mode);

    /**
     * Gets the type from the dirent type
     * @param type The type
     * @return The type
     */
    static unsigned char GetTypeFromDirent(FileType type);

    FS*    _FS;
    Inode* _data;
};

}