/*
 * MIT License
 *
 * Copyright (c) 2018 The Copper authors and contributors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <FileSystem/Ext/FS.hpp>
#include <Sync/Mutex.hpp>

namespace FileSystem::Ext
{

class Block
{
public:
    /**
     * Creates a new block
     * @param fs The filesystem
     * @param block The block
     * @param data The block data
     */
    Block(FS* fs, uint64_t block, uint8_t* data);
    ~Block();

    /**
     * Sync with device
     */
    void Sync();

    /**
     * Begins a write
     */
    void BeginWrite();

    /**
     * Ends a write
     */
    void EndWrite();

    /**
     * Increases the reference count
     */
    void IncRefCount();

    /**
     * Decreases the reference count
     */
    void DecRefCount();

private:
    FS* _FS;
    Sync::Mutex _writeLock;
    bool _dirty = false;

    uint32_t _refCount = 1;
    Sync::Mutex _refLock;
};

}