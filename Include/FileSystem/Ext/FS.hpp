/*
 * MIT License
 *
 * Copyright (c) 2018 The Copper authors and contributors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <FileSystem/MountableFileSystem.hpp>
#include <FileSystem/Ext/SuperBlock.hpp>
#include <DataStructure/HashMap.hpp>

namespace FileSystem::Ext
{

constexpr size_t BLOCK_HASHMAP_LENGTH = 32768;

class BlockGroup;
class ExtNode;
class Block;

class FS : public MountableFileSystem
{
public:
    ~FS();

    Node* GetRoot();
    dev_t GetDevNumber() const;
    bool Sync() const;
    uint32_t GetBlockSize() const;
    MountError Mount(const char* destination, const char* blockDevName, MountFlag flags);
    bool UnMount();

    /**
     * Gets a node from an inode
     * @param id The inode ID
     * @return The inode
     */
    ExtNode* GetNodeFromInode(uint32_t id);

    /**
     * Gets a block group by ID
     * @param id The ID
     * @return The block group
     */
    BlockGroup* GetBlockGroup(uint32_t id);

    /**
     * Gets a block by ID
     * @param id The ID
     * @return The block
     */
    Block* GetBlock(uint64_t id);

    /**
     * Reads a block
     * @param buffer The destination buffer
     * @param blockNum The block number
     * @param blockCount The number of blocks to read
     * @return True if the read was successful
     */
    bool ReadBlockDevice(void* buffer, uint64_t blockNum, uint32_t blockCount) const;

    /**
     * @param buffer The source buffer
     * @param blockNum The block number
     * @param blockCount The number of blocks to write
     * @return True if the write was successful
     */
    bool WriteBlockDevice(const void* buffer, uint64_t blockNum, uint32_t blockCount) const;

    /**
     * Check if we have a compat flag
     * @param feature The feature bit
     * @return True if we have the flag
     */
    inline bool HasCompat(uint32_t feature) const
    {
        return (_SB.GetFeatureCompat() & feature);
    }

    /**
     * Check if we have a compat flag
     * @param feature The feature bit
     * @return True if we have the flag
     */
    inline bool HasIncompat(uint32_t feature) const
    {
        return (_SB.GetFeatureIncompat() & feature);
    }

    /**
     * Check if we have a compat flag
     * @param feature The feature bit
     * @return True if we have the flag
     */
    inline bool HasROCompat(uint32_t feature) const
    {
        return (_SB.GetFeatureROCompat() & feature);
    }

    /**
     * Gets the inode size
     * @return The inode size
     */
    inline uint16_t GetInodeSize() const
    {
        return _SB.GetInodeSize();
    }

    /**
     * Gets the amount of inodes per group
     * @return Inodes per group
     */
    inline uint32_t GetInodesPerGroup() const
    {
        return _SB.GetInodesPerGroup();
    }

    /**
     * Gets the block group descriptor size
     * @return The block group descriptor size
     */
    inline uint32_t GetBGDescriptorSize() const
    {
        return _BGDescriptorSize;
    }

    /**
     * Checks if the filesystem is 64bit
     * @return True if 64bit
     */
    inline bool Is64() const
    {
        return _is64;
    }

private:
    /**
     * Checks if the filesystem is compatible
     * @return True if the filesystem is compatible
     */
    bool CheckCompatibility();

    struct SuperBlock _SB;
    uint32_t          _BGDescriptors;
    uint32_t          _BGDescriptorSize = 32;
    bool              _is64 = false;
    bool              _isReadOnly = false;

    Node*             _blockDevice = nullptr;
    Node*             _root;
    BlockGroup**      _blockGroups;

    // Block cache
    DataStructure::HashMap<uint64_t, Block*, DataStructure::NumberHasher<uint64_t>> _blockCache =
        DataStructure::HashMap<uint64_t, Block*, DataStructure::NumberHasher<uint64_t>>(BLOCK_HASHMAP_LENGTH);
};

}