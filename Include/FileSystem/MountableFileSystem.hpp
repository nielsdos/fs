/*
 * MIT License
 *
 * Copyright (c) 2018 The Copper authors and contributors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <FileSystem/IFileSystem.hpp>
#include <FileSystem/NodeContainer.hpp>
#include <Sync/Mutex.hpp>

namespace FileSystem
{

enum class MountError
{
    OK,
    INVALID_BLOCKDEV,
    INVALID_FS,
    INCOMPAT_FS,
    NOT_MOUNTED,
    NO_MEM
};

// Flags for mounting
typedef enum
{
    NONE      = 0,
    READ_ONLY = 1
} MountFlag;

/*
 * Every mountable filesystem needs to extend this.
 * Not all filesystems are mountable (e.g. NodeContainer is not mountable because it's completely virtual).
 */
class MountableFileSystem : public IFileSystem
{
public:
    /**
     * Mounts a filesystem
     * @param destination The destination mount name
     * @param blockDevName The name of the block device
     * @param flags The mount flags
     * @return The error status
     */
    virtual MountError Mount(const char* destination, const char* blockDevName, MountFlag flags) = 0;

    /**
     * Unmounts the filesystem
     * @return True if successfully unmounted
     */
    virtual bool UnMount();

protected:
    /**
     * Adds a mount point for this filesystem
     * @param destination The destination
     * @return The error status
     */
    ContainerError AddMountPoint(const char* destination);

    const char* _mountedOn = nullptr;
    Sync::Mutex _mountLock;
};

}