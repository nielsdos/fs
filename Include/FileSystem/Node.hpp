/*
 * MIT License
 *
 * Copyright (c) 2018 The Copper authors and contributors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <stddef.h>
#include <stdint.h>
#include <FileSystem/DirEnt.hpp>
#include <Sync/Mutex.hpp>

namespace FileSystem
{

struct Stat;

// TODO: fix this uglyness
#define OFF_T_MAX 0x7FFFFFFFFFFFFFFFLL

class IFileSystem;

class Node
{
public:
    virtual ~Node();

    /**
     * Open a node
     * @param name The name of the node
     * @return The node
     */
    virtual Node* Open(const char* name);

    /**
     * Closes a node
     */
    virtual void Close();

    /**
     * Gets the size of this node
     * @return The size
     */
    virtual off_t GetSize() const;

    /**
     * Obtain information of this node in a buffer
     * @param buf The buffer
     */
    virtual void GetStat(Stat* buf) const;

    /**
     * Change mode of a file
     * @param mode The new mode flags
     * @return True if succesful
     */
    virtual bool ChMod(mode_t mode);

    /**
     * Move the read/write file offset
     * @param oldOff The old file offset
     * @param newOff The new file offset
     * @return The new offset
     */
    virtual off_t Seek(off_t oldOff, off_t newOff);

    /**
     * Reads data from the node without updating offset
     * @param buffer The buffer to put the data into
     * @param count The size of the buffer
     * @param offset The offset
     * @return The amount of bytes read, -1 if error
     */
    virtual ssize_t PRead(void* buffer, size_t count, off_t offset);

    /**
     * Writes data to the node without updating offset
     * @param buffer The buffer to read the data from
     * @param count The size of the buffer
     * @param offset The offset
     * @return The amount of bytes written, -1 if error
     */
    virtual ssize_t PWrite(const void* buffer, size_t count, off_t offset);

    /**
     * Reads the directory entries
     * @param dirents The buffer where to put the directory entries into
     * @param count The amount of bytes to read
     * @param offset The offset in the directory
     * @return The amount of bytes read
     */
    virtual ssize_t ReadDirEntries(DirEnt* dirents, size_t count, off_t& offset);

    /**
     * Gets the block size
     * @return The block size
     */
    virtual uint32_t GetBlockSize() const;

    /**
     * Increases the reference count
     */
    void IncRefCount();

    /**
     * Decreases the reference count
     */
    void DecRefCount();

    /**
     * Returns the inode number
     * @return Inode number
     */
    inline ino_t GetIno() const
    {
        return _ino;
    }

    /**
     * Overrides the inode number (if not already set)
     * @param ino The inode number
     */
    void SetIno(ino_t ino);

    /**
     * Returns the file type
     * @return File type
     */
    inline unsigned char GetType() const
    {
        return _type;
    }

    /**
     * Gets the device number of this device node
     * @return The device number
     */
    virtual dev_t GetDevNumber() const;

    /**
     * Gets the filesystem of a node
     * @return The filesystem instance
     */
    virtual const IFileSystem* GetFS() const;

    /**
     * Overrides the filesystem of a node (if allowed)
     * @param fs The filesystem
     */
    virtual void SetFS(IFileSystem* fs);

protected:
    ino_t _ino = 0;
    unsigned char _type = DT_UNKNOWN;

private:
    uint32_t _refCount = 1;
    Sync::Mutex _refLock;
};

}