/*
 * MIT License
 *
 * Copyright (c) 2018 The Copper authors and contributors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <FileSystem/Node.hpp>
#include <FileSystem/Partition/MBR.hpp>
#include <Utils/Logger.hpp>

namespace FileSystem::Partition
{

struct MBRPartition
{
    uint8_t  Flags;
    uint8_t  StartHead;
    uint16_t StartSectorCylinder;
    uint8_t  SystemID;
    uint8_t  EndHead;
    uint16_t EndSectorCylinder;
    uint32_t StartSector;
    uint32_t TotalSectors;
} __attribute__((packed));

struct MBRHeader
{
    uint8_t      Bootstrap[446];
    MBRPartition Partitions[4];
    uint8_t      Signature[2];
};

static_assert(sizeof(MBRHeader) == 512);

MBR::MBR(Node* blockDevice)
    : IPartitionTable(blockDevice)
{
    if(!Init())
        return;

    MBRHeader mbr;
    if(blockDevice->PRead(&mbr, sizeof(mbr), 0) != sizeof(mbr))
        return;

    if(!IsTableValid(mbr))
        return;

    ParsePartition(mbr, 0);
    ParsePartition(mbr, 1);
    ParsePartition(mbr, 2);
    ParsePartition(mbr, 3);

    _valid = true;
}

bool MBR::IsTableValid(const MBRHeader& mbr) const
{
    if(mbr.Signature[0] != 0x55 || mbr.Signature[1] != 0xAA)
        return false;

    // There's a maximum of two extended partitions per MBR/EBR
    int extendedCount = 0;
    for(int i = 0; i < 4; i++)
    {
        const MBRPartition* part = &mbr.Partitions[i];
        if(part->StartSector == 0)
            continue;

        if(IsExtendedPartition(part))
            extendedCount++;
    }

    if(extendedCount > 2)
    {
        Logger::Warn("MBR: %d extended partitions, max 2 allowed", extendedCount);
        return false;
    }

    return true;
}

void MBR::ParsePartition(const MBRHeader& mbr, int n, off_t offset)
{
    const MBRPartition* part = &mbr.Partitions[n];
    if(part->StartSector == 0)
        return;

    // off_t is enough to hold (uint32_t max. value)², no need to check for overflow
    uint32_t blockSize = _blockDevice->GetBlockSize();
    off_t start = offset + (off_t)le32toh(part->StartSector) * blockSize;
    PartitionType type = (offset == 0) ? PartitionType::PRIMARY : PartitionType::LOGICAL;

    if(IsExtendedPartition(part))
    {
        MBRHeader ebr;
        if(_blockDevice->PRead(&ebr, sizeof(ebr), start) == sizeof(ebr) &&
            IsTableValid(ebr))
        {
            ParsePartition(ebr, 0, start);
            ParsePartition(ebr, 1, start);
            ParsePartition(ebr, 2, start);
            ParsePartition(ebr, 3, start);

            type = PartitionType::EXTENDED;
        }
        else
        {
            return;
        }
    }
    else if(IsEFIPartition(part))
        _hasEFISystemPartition = true;

    Partition partition
    {
        .Type = type,
        .Num = _currentID++,
        .Start = start,
        .Length = (off_t)le32toh(part->TotalSectors) * blockSize
    };

    AddPartition(partition);
}

bool MBR::IsExtendedPartition(const MBRPartition* part) const
{
    return (part->SystemID == 0x05 || part->SystemID == 0x0F || part->SystemID == 0x85);
}

bool MBR::IsEFIPartition(const MBRPartition* part) const
{
    return (part->SystemID == 0xEE);
}

}