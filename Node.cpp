/*
 * MIT License
 *
 * Copyright (c) 2018 The Copper authors and contributors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <errno.h>
#include <string.h>
#include <assert.h>
#include <FileSystem/Stat.hpp>
#include <FileSystem/Node.hpp>
#include <FileSystem/IFileSystem.hpp>

namespace FileSystem
{

Node::~Node()
{
}

Node* Node::Open(const char* /* name */)
{
    errno = ENOTDIR;
    return nullptr;
}

void Node::Close()
{
    DecRefCount();
}

off_t Node::GetSize() const
{
    return 0;
}

void Node::GetStat(Stat* buf) const
{
    memset(buf, 0, sizeof(Stat));

    buf->Inode     = _ino;
    buf->Size      = GetSize();
    buf->BlockSize = GetBlockSize();
    buf->RDevice   = GetDevNumber();

    auto fs = GetFS();
    if(fs)
        buf->Device = fs->GetDevNumber();
}

bool Node::ChMod(mode_t /* mode */)
{
    errno = EBADF;
    return false;
}

off_t Node::Seek(off_t /* oldOff */, off_t newOff)
{
    off_t size = GetSize();
    if(newOff > size)
        newOff = size;

    return newOff;
}

ssize_t Node::PRead(void* /* buffer */, size_t /* count */, off_t /* offset */)
{
    errno = EISDIR;
    return -1;
}

ssize_t Node::PWrite(const void* /* buffer */, size_t /* count */, off_t /* offset */)
{
    errno = EISDIR;
    return -1;
}

ssize_t Node::ReadDirEntries(DirEnt* /* dirents */, size_t /* count */, off_t& /* offset */)
{
    errno = ENOTDIR;
    return -1;
}

uint32_t Node::GetBlockSize() const
{
    return 0;
}

void Node::IncRefCount()
{
    _refLock.Lock();
    _refCount++;
    _refLock.Unlock();
}

void Node::DecRefCount()
{
    assert(_refCount > 0);
    _refLock.Lock();
    uint32_t old = _refCount--;
    _refLock.Unlock();
    if(old == 1)
        delete this;
}

void Node::SetIno(ino_t ino)
{
    // TODO: re-enable me when merging the kernel with kernel2
    // reason: assert & cassert clash .-.
    //assert(_ino == 0 && "Inode number already set");
    _ino = ino;
}

dev_t Node::GetDevNumber() const
{
    return 0;
}

const IFileSystem* Node::GetFS() const
{
    return nullptr;
}

void Node::SetFS(IFileSystem* /* fs */)
{
}

}