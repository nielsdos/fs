/*
 * MIT License
 *
 * Copyright (c) 2018 The Copper authors and contributors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <FileSystem/NodeContainer.hpp>

namespace FileSystem
{

class DeviceNode;

/**
 * Initializes the VFS
 */
void InitVFS();

/**
 * Generates a new unique major device number
 * @return Major device number
 */
uint16_t NewMajorDevNumber();

/**
 * Synchronize the volumes
 * @return True if succesfully synced
 */
bool Sync();

/**
 * Opens a node
 * @param node The starting node
 * @param name The relative path name
 * @return The node
 */
Node* Open(Node* node, const char* name);

/**
 * Opens a node
 * @param name The absolute path name
 * @return The node
 */
Node* Open(const char* name);

/**
 * Adds a volume
 * @param name The name
 * @param fs The filesystem
 * @return The error status
 */
ContainerError AddVolume(const char* name, IFileSystem* fs);

/**
 * Adds a new device
 * @param name The name
 * @param node The node
 * @return The error status
 */
ContainerError AddDevice(const char* name, Node* node);

/**
 * Removes a volume
 * @param name The volume name
 * @return True if the removal was successful
 */
bool RemoveVolume(const char* name);

/**
 * Removes a device
 * @param name The device name
 * @return True if the removal was successful
 */
bool RemoveDevice(const char* name);

/**
 * Creates a device number using a major and a minor number
 * @param major The major device number
 * @param minor The minor device number
 * @return The device number
 */
dev_t MakeDev(uint16_t major, uint32_t minor);

/**
 * Gets the major part of a device number
 * @return The major part of a device number
 */
uint16_t GetMajorFromDev(dev_t dev);

/**
 * Gets the major part of a device number
 * @return The major part of a device number
 */
uint32_t GetMinorFromDev(dev_t dev);

}