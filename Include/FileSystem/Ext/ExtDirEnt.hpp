/*
 * MIT License
 *
 * Copyright (c) 2018 The Copper authors and contributors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

namespace FileSystem::Ext
{

enum class FileType : uint8_t
{
    UNKNOWN = 0,
    REG,
    DIR,
    CHR,
    BLK,
    FIFO,
    SOCK,
    LNK 
};

constexpr size_t EXT_NAME_LEN = 255;

struct ExtDirEnt
{
    uint32_t Inode;
    uint16_t Reclen;
    uint8_t  Namelen;
    FileType Type;
    char     Name[0];

    /*
     * Conventient getters / setters.
     */

    inline uint32_t GetInode() const
    {
        return le32toh(Inode);
    }

    inline uint16_t GetReclen() const
    {
        return le16toh(Reclen);
    }

    inline uint8_t GetNamelen() const
    {
        return Namelen;
    }

    inline FileType GetType() const
    {
        return Type;
    }
};

struct HTreeEnt
{
    uint32_t Hash;
    uint32_t Block;     // Block number of the next node in the htree

    /*
     * Conventient getters / setters.
     */

    inline uint32_t GetHash() const
    {
        return le32toh(Hash);
    }

    inline uint32_t GetBlock() const
    {
        return le32toh(Block);
    }
};

struct HTreeRoot
{
    ExtDirEnt Dot;
    char      DotName[4];        // ".\0\0\0"
    ExtDirEnt DotDot;
    char      DotDotName[4];     // "..\0\0"
    uint32_t  Reserved;          // Reserved, zero atm
    uint8_t   HashVersion;       // See superblock
    uint8_t   InfoLength;        // Length of tree information
    uint8_t   IndirectionLevels; // Depth of HTree
    uint8_t   Flags;
    uint16_t  Limit;             // Maximum numbers of entries that can follow this header + 1
    uint16_t  Count;             // Number of entries that follow this header + 1
    uint32_t  Block;             // The block index that goes with hash = 0
    HTreeEnt  Entries[0];

    /*
     * Conventient getters / setters.
     */

    inline uint8_t GetHashVersion() const
    {
        return HashVersion;
    }

    inline uint8_t GetInfoLength() const
    {
        return InfoLength;
    }

    inline uint8_t GetIndirectionLevels() const
    {
        return IndirectionLevels;
    }

    inline uint8_t GetFlags() const
    {
        return Flags;
    }

    inline uint16_t GetLimit() const
    {
        return le16toh(Limit);
    }

    inline uint16_t GetCount() const
    {
        return le16toh(Count);
    }

    inline uint32_t GetBlock() const
    {
        return le32toh(Block);
    }
};

}