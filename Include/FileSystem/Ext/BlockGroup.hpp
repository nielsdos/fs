/*
 * MIT License
 *
 * Copyright (c) 2018 The Copper authors and contributors.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <stdint.h>
#include <endian.h>

namespace FileSystem::Ext
{

class FS;
class ExtNode;

struct BlockGroupDesc
{
    uint32_t BlockBitmapLo;
    uint32_t InodeBitmapLo;
    uint32_t InodeTableLo;
    uint16_t FreeBlocksCountLo;
    uint16_t FreeInodesCountLo;
    uint16_t UsedDirsCountLo;
    uint16_t Flags;
    uint32_t ExcludeBitmapLo;
    uint16_t BlockBitmapCSumLo;
    uint16_t InodeBitmapCSumLo;
    uint16_t ITableUnusedLo;
    uint16_t Checksum;
    /*
     * The following fields only exist if the 64 bit features is enabled
     * and DescriptorSize > 32.
     */
    uint32_t BlockBitmapHi;
    uint32_t InodeBitmapHi;
    uint32_t InodeTableHi;
    uint16_t FreeBlocksCountHi;
    uint16_t FreeInodesCountHi;
    uint16_t UsedDirsCountHi;
    uint16_t ITableUnusedHi;
    uint32_t ExcludeBitmapHi;
    uint16_t BlockBitmapCSumHi;
    uint16_t InodeBitmapCSumHi;
    uint32_t Reserved;

    /*
     * Conventient getters / setters.
     */

    inline uint32_t GetInodeTableLo() const
    {
        return InodeTableLo;
    }

    inline uint32_t GetInodeTableHi() const
    {
        return InodeTableHi;
    }
};

class BlockGroup
{
public:
    /**
     * Initializes a blockgroup
     * @param fs The filesystem
     * @param id The blockgroup ID
     * @param bgd The block group descriptor
     */
    BlockGroup(FS* fs, uint32_t id, BlockGroupDesc* bgd);
    ~BlockGroup();

    /**
     * Gets a node from an inode
     * @param id The inode ID
     * @return The inode
     */
    ExtNode* GetNodeFromInode(uint32_t id);

    /**
     * Sync
     */
    void Sync();

private:
    FS*      _FS;
    uint32_t _ID;
    uint32_t _offset;
    uint64_t _inodeTable;
    BlockGroupDesc* _data;
};

}